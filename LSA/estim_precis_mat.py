#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from lsa_job import *
from job_vars import JobVars
import yaml
from collections import defaultdict

from scipy import linalg
from sklearn.datasets import make_sparse_spd_matrix
from sklearn.covariance import GraphLassoCV, ledoit_wolf
import matplotlib.pyplot as plt
import networkx as nx
		
help_message = 'usage example: python kmer_cluster_merge.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

	#pDir = '/env/cns/proj/ADAM/scratch/Sawsan/resEGAD_nonNorm/'
	pDir = '/env/cns/proj/ADAM/LLDeep/PhyloMatrices/'
	print pDir


	rName = 'genus'
	#rName = 'species'
	#rName = 'strain'
	#rName = 'class'
	#rName = 'family'
	#rName = 'order'
	#rName = 'phylum'

	LS = glob.glob(pDir+'dicts/'+rName+'/*.dc')
	print len(LS)
	nbr_C = load_data(pDir+'dicts/'+rName+'/nbr_cols.txt',fl='nbr')
	print nbr_C
	
	S = dok_matrix((len(LS),nbr_C), dtype=float)
	sNames = []

	for i,fl in enumerate(LS):
		tD = load_data(fl,fl='list',dump='pk')
		sNames.append(fl[fl.rfind('/')+1:fl.rfind('.')])
		for k,v in tD.iteritems():
			S[i,k] = v

	pS0 = np.zeros(nbr_C,dtype=np.uint64)
	pS1 = np.zeros(len(LS),dtype=np.uint64)

	for ij,v in S.iteritems():
		pS0[ij[1]] += 1
		pS1[ij[0]] += 1

	nzi = np.nonzero(pS0)[0]
	X=np.zeros((len(LS),nzi.size),dtype=float)

	for ij,v in S.iteritems():
		j = np.where(nzi == ij[1])[0]
		X[ij[0],j] = v


	print X.shape
	X=X[np.sum(X,1)!=0,:]
	print X.shape
	X=X[:,np.sum(X,0)!=0]
	print X.shape

	#rule to chose taxID columns
	fl = np.sum(X!=0,0)>np.sqrt(X.shape[0])
	taxID = nzi[fl]
	X=X[:,fl]

	print X.shape
	sPath = pDir+'dicts/'+rName+'/taxID_len_'+str(len(taxID))+'/'
	if not os.path.isdir(sPath):
		os.makedirs(sPath)

	print sPath

	np.savetxt(sPath+'taxID.csv', taxID, delimiter=',',fmt='%i')   # X is an array
	np.savetxt(sPath+'sampleNames.csv', sNames, delimiter=',',fmt='%s')   # X is an array
	#sys.exit()

	X -= X.mean(axis=0)
	X /= X.std(axis=0)

	# #############################################################################
	# Estimate the covariance
	emp_cov = np.dot(X.T, X) / X.shape[0]
	print emp_cov.shape
	#sys.exit()

	t = time.time()
	nstp = 100
	#model = GraphLassoCV(cv=10,alphas = list(np.arange(1.0/nstp,1.0,1.0/nstp)))
	model = GraphLassoCV(cv=10,verbose=True)
	model.fit(X)
	elapsed = time.time() - t
	print 'time',elapsed

	cov_ = model.covariance_
	prec_ = model.precision_
	#print cov_
	#print prec_
	np.save(sPath+'cov.npy',cov_)
	np.save(sPath+'prec.npy',prec_)

	# #############################################################################
	# Plot the results
	plt.figure(figsize=(10, 3))
	plt.subplots_adjust(left=0.02, right=0.98)

	# plot the covariances
	covs = [('Empirical cov', emp_cov), ('GraphicalLassoCV cov', cov_),('GraphicalLasso prec', prec_)]
	vmax = cov_.max()
	for i, (name, this_cov) in enumerate(covs):
	    plt.subplot(1, 3, i + 1)
	    plt.imshow(this_cov, interpolation='nearest', vmin=-vmax, vmax=vmax,
		       cmap=plt.cm.RdBu_r)
	    plt.xticks(())
	    plt.yticks(())
	    plt.title(name)

	plt.savefig(sPath+'matrices.png')

	# plot the model selection metric
	print 'len(model.cv_alphas_)',len(model.cv_alphas_)
	plt.figure()
	plt.axes([.2, .15, .75, .7])
	plt.plot(model.cv_alphas_, np.mean(model.grid_scores_, axis=1), 'o-')
	plt.axvline(model.alpha_, color='.5')
	plt.title('Model selection: '+str(len(model.cv_alphas_)))
	plt.ylabel('Cross-validation score')
	plt.xlabel('alpha')
	plt.savefig(sPath+'cross_val.png')


	###########

	plt.figure()

	g = nx.Graph()

	thr = 0.0
	for i in range(prec_.shape[0]):
		for j in range(i,prec_.shape[0]):
			v = prec_[i,j]
			if np.abs(v)>thr:
				if v > 0:
					g.add_edge(i, j, weight=1)
				else:
					g.add_edge(j, i, weight=1)

	mx = np.max(np.abs(prec_))
	node_sizes = [200*prec_[i,i]/mx for i in range(prec_.shape[0])]

	pos = nx.circular_layout(g)
	edges = g.edges()
	weights = [int(10*np.abs(prec_[u,v])/mx) for u,v in edges]

	nx.draw_networkx_nodes(g, pos, node_size=node_sizes)
	nx.draw_networkx_labels(g,pos)
	nx.draw_networkx_edges(g, pos, node_size=node_sizes, arrowstyle='->', arrowsize=10,  width=weights)
 
	plt.title(rName+', GraphicalLasso precision')
	plt.axis('off')
	
	plt.savefig(sPath+'graph.png')

	plt.show()

	print sPath
