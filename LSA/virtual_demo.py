#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from lsa_names import get_file_name,get_dir_name

from lsa_job import *
from lsa_stats import filter_fasta_nodes,get_node_fasta
from job_vars import JobVars
from collections import defaultdict
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def get_N50_stat(LS):
	if len(LS) == 0:
		return 0,0
	LS.sort()
	hsm = sum(LS)/2
	i = 0
	csm = 0
	while csm < hsm:
		csm += LS[i]
		i += 1
	i -= 1
	N50 = LS[i]
	L50 = min(i,len(LS)-i)
	return N50,L50


def part_stat(p2res,p2spades,p2scaffolds):
	f = open_file(p2res+'part_stat.txt', 'w')

	part_nbr_reads = int(load_data(p2spades + 'merged_reads.nbr',fl='nbr'))
	part_nbr_ncl = int(JV.nbr_NR) * part_nbr_reads
	f.write('part_nbr_reads - '+str(part_nbr_reads)+'\n')
	f.write('part_nbr_ncl - '+str(part_nbr_ncl)+'\n')

	scaffolds_nodes = os.popen('cat '+ p2scaffolds +' | grep NODE').read().split()
	scaffolds_nbr_lines = int(os.popen('wc -l '+ p2scaffolds).read().split()[0])
	scaffolds_nbr_nodes = len(scaffolds_nodes)
	scaffolds_nbr_ncl  = file_size(p2scaffolds) - sum([len(val) for val in scaffolds_nodes]) - scaffolds_nbr_lines
	f.write('scaffolds_nbr_nodes - ' + str(scaffolds_nbr_nodes) + '\n')
	f.write('scaffolds_nbr_ncl - ' + str(scaffolds_nbr_ncl) + '\n')
	scaffolds_nodes_len = [int(val.split("_")[-3]) for val in scaffolds_nodes]
	scaffolds_N50,scaffolds_L50 = get_N50_stat(scaffolds_nodes_len)
	f.write('scaffolds_N50 - '+str(scaffolds_N50)+'\n')
	f.write('scaffolds_L50 - '+str(scaffolds_L50)+'\n')
	f.close()


def cg_ratio(p2scaffolds):

	path = p2scaffolds[:(p2scaffolds.rfind('/')+1)]
	nbr = p2scaffolds[(p2scaffolds.rfind('_')+1):p2scaffolds.rfind('.')]
	print path,p2scaffolds,nbr

	f = open(path + 'CG_ratio_cov_len.'+nbr+'.txt', 'w')
	M=np.zeros((0,3))
	for node_name,node_ACGT in get_node_fasta(p2scaffolds):
		node_CG_ratio = float((node_ACGT.count('C')+node_ACGT.count('G')))/(node_ACGT.count('C')+node_ACGT.count('G')+node_ACGT.count('A')+node_ACGT.count('T'))
		node_name = node_name.split('_')
		node_len = int(node_name[-3])
		node_cov = float(node_name[-1])
		f.write(str(node_CG_ratio) +',' + str(node_cov) + ',' + str(node_len)+'\n')
		M = np.vstack([M,[node_CG_ratio,node_cov,node_len]])
	f.close()
	sind =np.argsort(M[:,-1])
	M = M[sind,:]

	n = 50
	cm = plt.cm.get_cmap('jet')
	fig, ax = plt.subplots()

	mn = min(M[:,2])
	mx = max(M[:,2])
	if mx == mn: mn = 0

	colorIndex = (M[:,2]-mn)/(mx-mn)

	if M.shape[0] > 0:
		colorIndex = (M[:,2]-mn)/(mx-mn)

	sc = ax.scatter(M[:,0],M[:,1],s=M[:,2]/10.0,c=colorIndex,cmap=cm,linewidth=1,alpha=0.5)
	ax.grid()
	ax.set_xlim([0.0,1.0])

	fig.savefig(path + 'CG_ratio_cov_len.'+nbr+'.pdf',format='pdf')
	plt.close(fig)


def vt2files(p2file,gnb2gnm):

	psave = p2file[:(p2file.rfind('/')+1)]

	TB = load_data(p2file, fl='str')
	tb_cn=len(TB[-1].split(','))-1
	TBs = np.zeros(tb_cn)

	Nb2Nm = defaultdict(list)
	for ln in TB:
		tmp = ln.split(',')
		TBs += np.array([float(v) for v in tmp[1:]])

		gn = ln[:ln.find(',')]

		Nb2Nm[gnb2gnm[gn]].append(ln)

	for key,val in Nb2Nm.iteritems():
		nbrl = len(val)
		nbcl = len(val[0].split(','))-1
		M = np.zeros((nbrl,nbcl),dtype=np.int)
		names = []
		for i in range(len(val)):
			sval = val[i].split(',')
			names.append(sval[0])
			vc = sval[1:]
			vc = np.array([int(v) for v in vc],dtype=np.int)
			M[i,:] = vc
		pr = np.sum(M!=0,0)
		n_c = np.sum(pr!=0)
		n_u = np.sum((pr!=nbrl)*(pr>0))
		save_data(val,psave + 'nb2nm/' + str(nbrl) + '/' + str(n_c) + '_' + str(n_u) + '_' + key +'.txt',fl='str')
		thr_nbr = 1000

		fls = []
		for el in val:
			tmp = el.split(',')
			fal = np.array([float(v) for v in tmp[1:]])
			fal[fal<thr_nbr] = 0
			rfal = fal/TBs
			fls.append(tmp[0]+','+','.join(map(str,rfal)))
		save_data(fls,psave + 'nb2nm/' + str(nbrl) + '/' + str(n_c) + '_' + str(n_u) + '_' + key +'_normed'+'_thr_'+str(thr_nbr)+'.txt',fl='str')

		cm = plt.cm.get_cmap('jet')
		fig, ax = plt.subplots()

		M[M<thr_nbr] = 0
		mxx = np.max(M)
		rcl = np.random.rand(nbrl,3)
		for i in range(nbrl):
			ax.scatter(range(nbcl),i*np.ones((1,nbcl)),s=500*M[i,:]/mxx,c=rcl[i,:],linewidth=1,alpha=0.5)
			ax.text(nbcl+3, i, names[i],color=rcl[i,:])

		ax.grid(True)
		ax.set_xlim([-1,nbcl])
		ax.set_ylim([-1,nbrl])
		ax.set_title(key)
		ax.set_xlabel('cluster #')
		ax.set_ylabel('genome #')

         	plt.show()
		#sys.exit()
		fig.savefig(psave + 'nb2nm/' + str(nbrl) + '/' + str(n_c) + '_' + str(n_u) + '_' + key+'_thr_'+str(thr_nbr)+'.pdf',format='pdf')
		plt.close(fig)

def vt2bubbles(p2file,gnb2gnm):

	psave = p2file[:(p2file.rfind('/')+1)]

	TB = load_data(p2file, fl='str')
	tb_rn=len(TB)
	tb_cn=len(TB[-1].split(','))-1
	M = np.zeros((tb_rn,tb_cn))

	if (not os.path.isdir(psave + 'tb2bp_gn/')):
		os.system('mkdir -p ' + psave + 'tb2bp_gn/')
	if (not os.path.isdir(psave + 'tb2bp_pr/')):
		os.system('mkdir -p ' + psave + 'tb2bp_pr/')


	names = []
	for i in range(tb_rn):
		tmp = TB[i].split(',')
		names.append(tmp[0]+'_'+gnb2gnm[tmp[0]])
		M[i,:] = np.array([float(v) for v in tmp[1:]])
	print 'print genes'
	for i in range(tb_rn):
		cm = plt.cm.get_cmap('jet')
		fig, ax = plt.subplots()

		mxx = np.max(M[i,:])
		rcl = np.random.rand(tb_cn,3)
		ax.scatter(range(tb_cn),np.ones((1,tb_cn)),s=500*M[i,:]/mxx,c=rcl,linewidth=1,alpha=0.5)

		ax.grid(True)
		ax.set_xlim([-1,tb_cn])
		ax.set_ylim([0,2])
		ax.set_title(names[i])
		ax.set_xlabel('partition #')
		ax.set_ylabel('genome')
         	plt.show()
		fig.savefig(psave + 'tb2bp_gn/' + str(i) + '_' + names[i]+'.pdf',format='pdf')
		plt.close(fig)

	print 'print parts'
	for j in range(tb_cn):
		cm = plt.cm.get_cmap('jet')
		fig, ax = plt.subplots()

		mxx = np.max(M[:,j])
		rcl = np.random.rand(tb_rn,3)
		ax.scatter(range(tb_rn),np.ones((1,tb_rn)),s=500*M[:,j]/mxx,c=rcl,linewidth=1,alpha=0.5)

		ax.grid(True)
		ax.set_xlim([-1,tb_rn])
		ax.set_ylim([0,2])
		ax.set_title('part #' + str(j))
		ax.set_xlabel('genome #')
		ax.set_ylabel('partition')
         	plt.show()
		fig.savefig(psave + 'tb2bp_pr/' + str(j) + '.pdf',format='pdf')
		plt.close(fig)


help_message = 'usage example: python script_name.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg


	p2genes = '/env/cns/proj/ADAM/scratch/Data/virtual/virtial_cohort_700_genomes.lst'
	gTB = load_data(p2genes,fl='str')

	print len(gTB)
	gnb2gnm = {}
	gnmD = defaultdict(int)
	for st in gTB:
		gene_nb = st[(st.find('_')+1):st.find('.')]
		gene_nm = st[(st.find(';')+1):]
		gene_nm = gene_nm.split()
		gene_nm = gene_nm[0]+'_'+gene_nm[1]
		gnb2gnm[gene_nb] = gene_nm
		gnmD[gene_nm] += 1
	print len(gnb2gnm)
	gnmL = gnmD.keys()
	print len(gnmL)
	print gnb2gnm['000917']
	
	lFiles = get_file_list(pDir,'','','virtual_table.txt')
	print lFiles

	lFiles = get_file_list(pDir,'','','virtual_table.txt')
	print lFiles
	for p2file in lFiles:
		print p2file
		vt2bubbles(p2file,gnb2gnm)

