#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars
from collections import defaultdict
from space_m import  mat_count_connects,uv_un_D
from scipy.sparse import dok_matrix

help_message = 'usage example: python script.py -r 0 -p /project/home/'

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in '-h':
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')

	HO = StreamingEigenhashes(JV.oDir)
	model = HO.load_decomp_model()

	HO.open_AM_files()

	savedir = JV.oDir + JV.sdrTMP + HO.dec_cls_dir + 'IND/'
	read_dir = HO.oDir + HO.sdrTMP + HO.dec_cls_dir
	X = [load_data(read_dir + 'X_'+str(i),fl='mm',dump=np.float32) for i in range(HO.nbr_T)]
	VI = np.memmap(read_dir+'VI', dtype = np.uint64, mode='r', shape=(HO.nbr_H,))
	IND = np.memmap(read_dir+'IND', dtype = np.uint64, mode='r', shape=HO.szIndMat())

	mnb = HO.ms_m()
	print 'filter m,mnb',mnb

	uv,un,D = uv_un_D(VI)
	cl = uv.size

	CN = dok_matrix((cl, cl), dtype=np.uint64)
    
	for k in range(step):
		ind = step*fr + k
		if ind >= HO.nbr_C:
			break

		strC = ind*HO.cMax
		szC = min(HO.cMax,HO.nbr_H-strC)

		LS = xrange(strC,strC+szC)
		tCN = mat_count_connects(X,IND,VI,LS,mnb)

		CN += tCN
	print 'job - ',fr
	del VI,IND
	D={}
	for k,v in CN.items():
		D[k]=v
	save_data(D,read_dir+'tCN/'+str(fr)+'.cn',fl='list',dump='pk')
	HO.close_AM_files()		
