#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from lsa_job import *
from lsa_stats import SafeRt

def sm_size(FL):
        s=0
        if len(FL) != 0:
            print FL[0][:FL[0].rfind('/')]
            for fl in FL:
		    s += file_size(fl)
        return s

def sm_lines(FL):
        sl = 0
        if len(FL) != 0:
            print FL[0][:FL[0].rfind('/')]
            for fl in FL:
                sl += file_size(fl)
        return sl

def get_clusters_stats(path):

        MAS = load_nCLS(path)

        print MAS
        S=[]
        SL=[]
        for i in range(MAS):
            FL=glob.glob(path+str(i+1) + '/*.fastq')
            S.append(sm_size(FL))
            SL.append(sm_lines(FL))
        ST=[str(i+1)+'\t'+str(S[i]) + '\t' +str(SL[i])+'\n' for i in range(MAS)]
        f=open_file(path+'cluster_stats.txt','w')
        f.writelines(ST)
        f.close()

def get_partitions_stats(path):
        S=[]
        SL=[]
        NM=[]
        FL=glob.glob(path+ '/*.fastq')
        for i in range(len(FL)):
            fl=path+'cluster_'+str(i+1)+'.fastq'
            S.append(sm_size(fl))
            SL.append(sm_lines(fl))
            NM.append(fl[fl.rfind('/')+1:])
        ST=[NM(i)+'\t'+str(S[i]) + '\t' +str(SL[i])+'\n' for i in range(MAS)]
        f=open_file(path+'partition_stats.txt','w')
        f.writelines(ST)
        f.close()

def show_dir_size(path):
        FL=glob.glob(path + '*.fastq')
        s=sm_size(FL)
        sl=sm_lines(FL)
        print 's - ',s,' sl - ',sl

def get_samples_size(path,lines=True):
        LS = glob.glob(path + '/SRS*')
        LS.sort()
        if len(LS)==0:
            print 'the number of samples is  0 !!!!!!!'
            return
        s=[]
        sl=[]
        for i in range(len(LS)):
            print '--------------------------------------------'
            print 'i - '+str(i)+' , start sample ' + LS[i][LS[i].rfind('/')+1:]
            FL=glob.glob(LS[i] + '/*.fastq*')

            s.append(sm_size(FL))
            print 's - ',s[-1]
            if lines:
                sl.append(sm_lines(FL))
                print ' sl - ',sl[-1]
            if s[-1]==0:
                print 'empty sample',
                return
        print '-------------------------------------'
        fname='samples_stats.txt'
        print 'done ! stats in the file in'
        print path+fname
        if lines:
            ST=[str(i)+'\t'+ LS[i][LS[i].rfind('/')+1:] + '\t' +str(s[i]) + '\t' +str(sl[i]) + '\t' +str(sl[i]/4) + '\n' for i in range(len(s))]
        else:
            ST=[str(i)+'\t'+ LS[i][LS[i].rfind('/')+1:] + '\t' +str(s[i])  + '\n' for i in range(len(s))]

        ST.append('total size in bytes: ' + str(sum(s)) + '\n')
        if lines:
            ST.append('total lines in files: ' + str(sum(sl)) + '\n')
            ST.append('total reads in files: ' + str(sum(sl)/4) + '\n')
        f=open_file(path+fname,'w')
        f.writelines(ST)
        f.close()

def compare_init_merged(path1,path2):
        LS1 = glob.glob(path1 + '/SRS*')
        LS2 = glob.glob(path2 + '/SRS*')
        LS1.sort()
        LS2.sort()
        if len(LS1)!=len(LS2):
            print 'the number of samples is different!!!!!!!'
            return
        sl1=[]
        sl2=[]
        for i in range(len(LS1)):
            print '--------------------------------------------'
            print 'i - '+str(i)+' , start sample ' + LS1[i][LS1[i].rfind('/')+1:]
            FL1=glob.glob(LS1[i] + '/*.fastq*')
            FL2=glob.glob(LS2[i] + '/*.fastq*')
            s1 = sm_size(FL1)
            s2 = sm_size(FL2)
            sl1.append(s1)
            sl2.append(s2)
            print 's1 - ',s1,' s2 - ',s2
            if s1!=s2:
                print 'Check the data !!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
                return
        print '-------------------------------------'
        print 'done ! all data are in the same size'
        ST=[str(i)+'\t'+str(sl1[i]) + '\t' +str(sl2[i])+'\n' for i in range(len(sl1))]
        f=open_file(path1+'cluster_stats.txt','w')
        f.writelines(ST)
        f.close()

def check_paired_files(path):
        print 'check if paired files exist in'
        print path
        LS = glob.glob(path + 'SRS*')
        if len(LS)==0:
            print 'no samples, exit'
            return
        LS.sort()
        SL=[]
        for i in range(len(LS)):
            FL=glob.glob(LS[i] + '/*.fastq*')
            if len(FL)%2 != 0:
                print '--------------------------------------------'
                print 'i - '+str(i)+' , start sample ' + LS[i][LS[i].rfind('/')+1:]
                print 'no paired fasq file'
                return
        print 'done OK--------------------------------------------'

def cat_fastq_partitions(path):
        print path
        os.system('rm -f '+path+'/prt*.fastq')

        LS = glob.glob(path+'*')
        if len(LS)==0:
            print 'no partitions, exit in\n',path
            return
        for i in range(len(LS)):
            FL=glob.glob(LS[i] + '/*.fastq')
            if len(FL) != 0:
                print '--------------------------------------------'
                print 'i - '+str(i)+' , start partition ' +LS[i]
                print get_dir_name([LS[i]])[0]
                os.system('cat '+' '.join(FL)+' > '+ get_dir_name([LS[i]])[0] +'/prt_' + get_file_name([LS[i]])[0]+'.fastq')
            os.system('rm -r '+ LS[i])

        print 'done OK--------------------------------------------'

def print_partition_length(path):

        LS = glob.glob(path+'/*.cls')
	LS.sort()
        V=[]
        NM=[]

        for cl in LS:
		print 'strat porcessing part #: '+ str(cl)
		name = get_file_name(cl)
		name = cut_sufix_from_name(name)
		IND = load_data(cl,fl='list')
		ln = sum([len(v) for v in IND])
		V.extend([ln])
		NM.extend([name])
		del IND
		print cl
	
	OUT=[]
	OUT.extend(['---------------------------------'])
	OUT.extend(['sorted partitions:'])
        ind = sorted(range(len(V)), key=lambda k: V[k])
        for i in range(len(LS)):
            OUT.extend(['# of reads for partition ' + str(NM[ind[i]]) + ' - ' + str(V[ind[i]])])
	save_data(OUT,path+'pr_size.txt',fl='str')
	print 'done!'
	print 'data saved into ' + path


def print_parts_inters(pPrt,uPrt):

        pLS = glob.glob(pPrt+'/*.cls')
        uLS = glob.glob(uPrt+'/*.cls')
	if (len(pLS) != len(uLS)) and (len(pLS) != 0):
		raise NameError('different number of partitions !')

	OUT=[]
	OUT.extend(['intersection for partitions:'])
	OUT.extend([pPrt])
	OUT.extend([uPrt])
	OUT.extend(['---------------------------------'])
	for cl in xrange(len(pLS)):
		pIND = load_data(pPrt+str(cl)+'.cls',fl='list')
		uIND = load_data(uPrt+str(cl)+'.cls',fl='list')
		if len(pIND)!=len(uIND):
			raise NameError('different number of files for partition ' + str(cl))
		pN = 0
		uN = 0
		iN = 0
		for fi in xrange(len(pIND)):
			eP=[2*v+0 for v in pIND[fi]] + [2*v+1 for v in pIND[fi]]
			uP=uIND[fi]
			iP = list(set(eP).intersection(uP))
			pN += len(eP)
			uN += len(uP)
			iN += len(iP)
		st = 'class '+str(cl)+' : intersection ratio - '+ str(float(2*iN)/(pN+uN))
		print st
		OUT.extend([st])

	save_data(OUT,pPrt+'pr_intrs.txt',fl='str')
	save_data(OUT,uPrt+'pr_intrs.txt',fl='str')
	print 'done!'
	print 'data saved into ' + pPrt
	print 'data saved into ' + uPrt

def check_odd_parts(pPrt):

        LS = glob.glob(pPrt+'/*.cls')
	if (len(LS) == 0):
		raise NameError('different number of partitions !')

	OUT=[]
	OUT.extend(['check pairs in parts:'])
	OUT.extend([pPrt])
	OUT.extend(['---------------------------------'])
	for cl in xrange(len(LS)):
		pIND = load_data(pPrt+str(cl)+'.cls',fl='list')
		pN = 0
		sN = 0
		for fi in xrange(len(pIND)):
			pI=[v/2 for v in pIND[fi]]
			sI = list(set(pI))
			pN += len(pI)
			sN += len(sI)
		#print cl,pN,sN
		del pIND
		st = 'part # '+str(cl)+', # of reads - '+str(pN)+', # of reads set - '+str(sN)+', ratio of pairs - '
		if pN == 0:
			st += '0.0'
		else:
			st +=str(float(2*(pN-sN))/pN)
		print st
		OUT.extend([st])

	save_data(OUT,pPrt+'pr_unpaired_stat.txt',fl='str')
	print 'done!'
	print 'data saved into ' + pPrt

def get_clasters_lines(pDir):
         JobVars = load_data(pDir,fl='job')
         FASTQ = load_data(JobVars['oDir'] +JobVars['sdrCLS'] + 'fileList.txt',fl='str')

def fasta2cov(p2f):
	f = open_file(p2f)
	LS=[]
	with open_file(p2f) as f:
		for line in f:
			if 'NODE' in line:
				tmp = line[1:-1]
				LS.append(tmp)
	return LS

def combine_spades_checkm_part_stat(path):
	LS = glob.glob(path + 'spades_*/')
	LS.sort()
	f = open_file(path+'spades_checkm_stat.txt', 'w')
	f.write('prt, p_#_reads, p_#_ncl, s_#_nodes, s_#_ncl, s_N50, s_L50, #_g, #_m, #_m_s, 0, 1, 2, 3, 4, 5+, Compl, Contam, S_homog\n')

	for i in range(len(LS)):
		print '--------------------------------'
		print len(LS),' - ',i,', current partition - ',LS[i]
		part_nbr = LS[i][(LS[i].rfind('_')+1):-1]
		path2spades = LS[i] + 'part_stat.txt'
		path2checkm = path[:path.rfind('partitions')]+'logs/' + 'Checkm_prt_' + part_nbr + '.slurm.out'
		print path2spades
		print path2checkm
		empty = False
		if (not os.path.isfile(path2spades)) or (not os.path.isfile(path2checkm)):
			empty =True
		elif (os.stat(path2spades).st_size == 0) or (os.stat(path2spades).st_size == 0):
			empty =True
		if empty:
			print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
			print path2spades
			print path2checkm
			print 'some files do not exist in '+ LS[i]
			print 'next step'
			continue

		with open_file(path2spades,'r') as tmp:
			spades = tmp.read().splitlines()
		checkm = os.popen('cat '+ path2checkm +' | grep "scaffolds" | tail -1').read().split()

		string2write = part_nbr
		string2write += ''.join([', '+val.split()[-1] for val in spades])
		string2write += ', '
		string2write += ', '.join(checkm[3:])
		string2write += '\n'
		f.write(string2write)

	f.close()


def split_fasta_partitions2samle_files(path2file):

	for node_name,node_ACGT in get_node_fasta(path2file + 'scaffolds.fasta'):
		node_name_blocks = node_name.split('_')
		part_nbr = node_name_blocks[1]
		nbr = int(part_nbr)
		path2part = path2file +'spades_prt_'+ part_nbr + '/'

def get_sparse_stat_memmap(path):

	path2save = path + 'stat/'
	os.system('mkdir -p '+ path2save)
	f = open_file(path2save + 'stat.txt', 'w')

	from counts_vector import Counts_Vector

	print path
	spl = path.split('_')

	kMer = int(spl[-2][1:])
	f.write('kMer - ' + str(kMer) + '\n')
	hSize = int(spl[-1][1:-1])
	f.write('hSize - ' + str(hSize) + '\n')
	hash_max = 2**hSize
	f.write('hash_max value- ' + str(hash_max) + '\n')

	cMax = 0
	if hSize >= 10:
		cMax = hSize - 10
	
	f.write('cMax - ' + str(cMax) + '\n')

	nbr_chunks = 2**(hSize - cMax)
	f.write('# of chunks - ' + str(nbr_chunks) + '\n')
	
	block_size = 2**cMax
	f.write('block_size - ' + str(block_size) + '\n')
	G = Counts_Vector()

	import matplotlib.pyplot as plt

	LS = glob.glob(path + '*.nzi')
	f.write('# of samples - ' + str(len(LS)) + '\n')
	for i in xrange(len(LS)):
		print i,len(LS)	
		I = np.memmap(LS[i],dtype=np.uint64,mode='r')
		tI=[int(v) for v in I]
		H =[0]*nbr_chunks
		for el in tI:
			H[el/block_size] += 1

		fig, ax = plt.subplots()
		ax.stem(range(nbr_chunks), H)
		fig.savefig(path2save + 'hash_cMax_smp_' + str(i) + '.png',format='png')
		plt.close(fig)

		md_i = len(tI)/2
		f.write('smp - ' + str(i) + '; n-z el. - ' + str(len(tI)) + '; % of n-z el. to hash-size - ' + str(float(len(tI))/hash_max) + '; median - ' + str(SafeRt(tI[md_i],hash_max))  + '\n')
		G.add_IV_fast(tI,[1]*len(I))
		del I
		del tI

	H =[0]*nbr_chunks
	for el in G.I:
		H[el/block_size] += 1

	fig, ax = plt.subplots()
	ax.stem(range(nbr_chunks), H)
	fig.savefig(path2save + 'hash_cMax_global.png',format='png')
	plt.close(fig)

	md_i = len(G.I)/2
	f.write('global by columns' + ' n-z el. - ' + str(len(G.I)) + '; % of n-z el. to hash-size - ' + str(float(len(G.I))/hash_max) + '; median - ' + str(SafeRt(G.I[md_i],hash_max))  + '\n')
	f.close()
	print 'information saved into ',path2save


def get_int_size():
	n = 20

	print '%3s%15s%15s%15s' % ('#','# of hashes', '# of decs', '# of bytes')
	for i in xrange(n+1):
		pr = 2**i
		v = 2**pr
		sz = sys.getsizeof(v)
		ln =len(str(v))
		print '%3d%15d%15d%15d' % (i,pr, ln, sz)



def get_sparse_stat_memmap_prost(path):

	
	path2save = path + 'stat/'
	os.system('mkdir -p '+ path2save)
	f = open_file(path2save + 'stat.txt', 'w')

	from counts_vector import Counts_Vector

	print path

	kMer = 33#int(spl[-2][1:])
	f.write('kMer - ' + str(kMer) + '\n')
	hSize = 22#int(spl[-1][1:-1])
	f.write('hSize - ' + str(hSize) + '\n')
	hash_max = 2**hSize
	f.write('hash_max value- ' + str(hash_max) + '\n')

	cMax = 0
	if hSize >= 10:
		cMax = hSize - 10
	
	f.write('cMax - ' + str(cMax) + '\n')

	nbr_chunks = 2**(hSize - cMax)
	f.write('# of chunks - ' + str(nbr_chunks) + '\n')
	
	block_size = 2**cMax
	f.write('block_size - ' + str(block_size) + '\n')
	G = Counts_Vector()

	import matplotlib.pyplot as plt

	LS = glob.glob(path + 'hashes/*.nonzero.npy')

	f.write('# of samples - ' + str(len(LS)) + '\n')
	MX = []
	for i in xrange(len(LS)):
		print i,len(LS)	
		I = np.load(LS[i])
		tI=[int(v) for v in I]
		print i,'max ind val',max(tI)
		MX.append(max(tI))

		H =[0]*nbr_chunks
		for el in tI:
			H[el/block_size] += 1

		fig, ax = plt.subplots()
		ax.stem(range(nbr_chunks), H)
		fig.savefig(path2save + 'hash_cMax_smp_' + str(i) + '.png',format='png')
		plt.close(fig)

		md_i = len(tI)/2
		f.write('smp - ' + str(i) + '; n-z el. - ' + str(len(tI)) + '; % of n-z el. to hash-size - ' + str(float(len(tI))/hash_max) + '; median - ' + str(SafeRt(tI[md_i],hash_max))  + '\n')
		G.add_IV_fast(tI,[1]*len(I))
		del I
		del tI

	print 'max ind val',max(MX)
	H =[0]*nbr_chunks
	for el in G.I:
		H[el/block_size] += 1

	fig, ax = plt.subplots()
	ax.stem(range(nbr_chunks), H)
	fig.savefig(path2save + 'hash_cMax_global.png',format='png')
	plt.close(fig)

	md_i = len(G.I)/2
	f.write('global by columns' + ' n-z el. - ' + str(len(G.I)) + '; % of n-z el. to hash-size - ' + str(float(len(G.I))/hash_max) + '; median - ' + str(SafeRt(G.I[md_i],hash_max))  + '\n')
	f.close()
	print 'information saved into ',path2save


help_message = 'usage example: python script_file.py -p /project/home/'
if __name__ == "__main__":

	#path = '/env/cns/proj/ADAM/scratch/Abundance_Matrices/PRJNA288027/LSA_k33_h30/'
	#get_sparse_stat_memmap(path)
	
	#path = '/env/cns/bigtmp1/okyrgyzo/prost/NMF/'
	#get_sparse_stat_memmap_prost(path)
	
	'''
	path = '/env/cns/bigtmp1/okyrgyzo/plusSpike/LSA_k33_h32/'
	get_sparse_stat_compressed_list(path)
	
	#get_int_size()

	'''

	#path = '/env/cns/proj/ADAM/scratch/PRJNA288027/Olexiy/spades_stat/'
	'''
	#path = '/env/cns/proj/ADAM/scratch/PRJNA288027/LSA_k33_h32/partitions/thr_0.9/'
	for val in [17  , 260  , 297  , 360  , 396  , 416  , 432  , 460  , 464  , 552  , 590  , 689  , 73]:
		path = '/env/cns/bigtmp1/okyrgyzo/PRJNA288027_tests_bull/LSA_k33_h32/partitions/thr_0.9/prt_' +str(val)+ '/LSA_k33_h32/partitions/thr_0.9/'
		combine_spades_checkm_part_stat(path)
	'''

	'''
	path = '/env/cns/proj/ADAM/scratch/PRJNA288027/Olexiy/spades_stat/'
	split_fasta_partitions2sample_files(path)
	'''

    #path = '/env/cns/proj/projet_BWG/scratch/merged/'
    #path = '/genoscope/merged/'
    #path='/genoscope/test_3sm/init/'
	'''
	prt='192'
	p2f='/env/cns/bigtmp1/okyrgyzo/parts/spades_prt_'+prt+'/contigs.fasta'
	p2s = '/env/cns/bigtmp1/okyrgyzo/parts/spades_prt_'+prt+'/prt_'+prt+'_contigs.cov'
	LS = fasta2cov(p2f)
	save_data(LS,p2s,fl='str')
	'''
	'''
	#uPrt = '/genoscope/test_all/LSA_k30_h30/clusters/thr_0.9/INDX/'
	uPrt = '/env/cns/bigtmp1/okyrgyzo/PRJNA288027/LSA_k30_h30/clusters/thr_0.9/INDX/'
	check_odd_parts(uPrt)
	'''

	'''
	pPrt = '/genoscope/test_all/clusters/paired_thr_0.5/INDX/'
	uPrt = '/genoscope/test_all/clusters/unpaired_thr_0.5/INDX/'
	print_parts_inters(pPrt,uPrt)
	'''
	'''
	path = '/genoscope/test_all/LSA_k30_h30/clusters/thr_0.9/INDX/'
	print_partition_length(path)
	'''

        #path = '/env/cns/proj/projet_BWG/scratch/merged/'
        #path = '/genoscope/merged/'
        #path='/genoscope/test_3sm/init/'
        #get_samples_size(path,lines=False)

        
	#ps = '/env/cns/bigtmp1/okyrgyzo/plusSpike/LSA_k33_h22/partitions/thr_0.8/'
        #cat_fastq_partitions(ps)
        

        '''
        path = '/env/cns/bigtmp1/okyrgyzo/merged_pair1/clusters/thr_0.9/'
        get_clusters_stats(path)
        path = '/env/cns/bigtmp1/okyrgyzo/merged_pair1/partitions/thr_0.9/'
        get_partitions_stats(path)
        '''

        '''
        #path1 = '/env/cns/bigtmp1/okyrgyzo/cmp/1/SRS475568'
        #path2 = '/env/cns/bigtmp1/okyrgyzo/cmp/2/SRS475568'
        path1 = '/env/cns/bigtmp1/okyrgyzo/merged_pair1/partitions/thr_0.9/9/'
        path2 = '/env/cns/bigtmp1/okyrgyzo/merged_pair1/clusters/thr_0.9/9/'
        show_dir_size(path1)
        show_dir_size(path2)
        '''

        '''
        path1 = '/env/cns/proj/projet_BWG/scratch/'
        path2 = '/env/cns/proj/projet_BWG/scratch/merged/'
        compare_init_merged(path1,path2)
        '''

        '''
        path1 = '/genoscope/merged/'
        path2 = '/genoscope/'
        compare_init_merged(path1,path2)
        '''

        '''
        #path = '/env/cns/proj/projet_BWG/scratch/'
        path = '/genoscope/'
        check_paired_files(path)
        '''
