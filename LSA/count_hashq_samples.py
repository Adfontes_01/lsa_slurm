#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from fastq_reader import Fastq_Reader
from lsa_job import *
from lsa_stats import SafeRt
from job_vars import JobVars

help_message = 'usage example: python merge_count_fractions.py -r 3 -p /project/home'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('samples')
	FP = load_data(JV.oDir + JV.sdrJOB + 'sampleList.txt',fl='str')

	HO = Fastq_Reader(JV.oDir)
	
	tp=np.uint16
	type_max = int(np.iinfo(tp).max)
	for i in range(step):
		ind = step*fr + i
		if ind >= HO.nbr_S:
			break
		sample_name = FP[ind]
		path2sample = HO.oDir + HO.sdrHASH +  sample_name
		path2dict = path2sample + '/DICT/'

		print 'sample - ',sample_name
		H = HO.hash_counts_for_sample_add_dicts(sample_name)
		
		save_data(H.I,path2sample + '.count.hash.nzi',fl='list')

		H.V = [v if (v <= type_max) else type_max for v in H.V]
		H = np.array(H.V,dtype = tp)

		save_data(H,path2sample + '.count.hash',fl='mm')

		ln = H.size
		rt = SafeRt(ln,HO.hMax) 

		print 'sample %s has %d nonzero elements and %d total observed kmers with sparseness %f' % (sample_name,ln,H.sum(), 1 - rt)
		remove_dir(path2dict)

