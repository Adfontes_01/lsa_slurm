#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from fastq_reader import Fastq_Reader
from lsa_job import *

help_message = 'usage example: python merge_count_fractions.py -r 3 -p /project/home'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-r','--filerank'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg
			if pDir[-1] != '/':
				pDir += '/'


        JobVars = load_data(pDir,fl='job')
	FP = load_data(JobVars['oDir'] + JobVars['sdrJOB'] + 'fileList.txt',fl='str')
	AS,step = array_size_step(int(JobVars['nbr_S']),int(JobVars['MAS']))

	FP = [fp[fp.rfind('/')+1:fp.index('.CH')] for fp in FP]
	FP = list(set([fp[:fp.index('.')] for fp in FP]))

	for i in range(step):
		ind = step*fr + i
		if ind >= len(FP):
			break

		file_prefix = FP[ind]
		hashobject = Fastq_Reader(JobVars['oDir'])
		hashobject.merge_count_fractions(file_prefix)
