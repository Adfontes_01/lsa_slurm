#!/usr/bin/env python

import sys,getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from gensim import models
import time
from scipy.spatial import distance
import math
from job_vars import JobVars
from lsa_math import safe_cosine

help_message = 'usage example: python kmer_vectors.py -p /project/home/ -n 8 -d'
if __name__ == "__main__":

	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')
	HO = StreamingEigenhashes(JV.oDir)

	svDir = JV.oDir + JV.sdrCLS

	fName = svDir + 'F.npy'
	F = load_data(fName,fl='npy')

	fName = svDir + 'BLK.blk'
	BLK = load_data(fName,fl='list')

	fName = svDir + 'OUT.out'
	OUT = load_data(fName,fl='list')

	fName = svDir + 'C.c'
	C = load_data(fName,fl='list')

	print
	n = len(C)
	print 'len(C)',len(C),HO.nbr_H,n
	print 'merge data points to locals'
	for cl in xrange(n):
		print str(cl),' - ',n

		ind = cl
		T = [ind]
		val = F[ind]
		while val < F[C[ind]]:
			T += [C[ind]]
			ind = C[ind]
			val = F[ind]
		if len(T) == 1:
			print F[T[0]]
		for v in T:
			C[v] = T[-1]

	print

	CLS = list(set(C))
	print "# of clusters",len(CLS)
	SZ =[]
	for cls in CLS:
		fl = []
		for j in xrange(len(C)):
			if C[j] == cls:
				fl += [j]
		print cls,'-',len(fl)
		SZ += [len(fl)]

	print sum(SZ),HO.nbr_H
	
	print "# of clusters",len(CLS)
	print sorted(SZ,reverse=True)
	print sorted(CLS)
	print len(CLS)
