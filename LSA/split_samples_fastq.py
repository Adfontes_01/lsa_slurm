#!/usr/bin/env python
import sys,os,glob
import getopt,gzip
from lsa_job import *
from lsa_names import *
from fastq_reader import Fastq_Reader
from job_vars import JobVars


if __name__ == "__main__":
        try:
                opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
        except:
                print help_message
                sys.exit(2)

        for opt, arg in opts:
                if opt in ('-h'):
                        print help_message
                        sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	FP = load_data(JV.oDir + JV.sdrJOB + '/fileList.txt',fl='str')

	if type(FP) is not list:
		FP = [FP]
	AS,step =JV.get_array_size('files')
	FO = Fastq_Reader(JV.oDir)

	for i in range(step):
		ind = step*fr + i
		if ind >= len(FP):
			break
		print '---------------------------------'
		print FP[ind], 'to split par samples'
		FO.split_samples_fastq(ind)
		print FP[ind], 'was splitted'

	print fr,'is over, samles were saved into ', FO.oDir +  FO.sdrSAMPLES
