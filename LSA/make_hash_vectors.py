#!/usr/bin/env python

import sys,getopt,os,glob,time
from fastq_reader import Fastq_Reader
from lsa_job import save_data
from job_vars import JobVars

help_message = 'usage example: python hash_fastq_reads.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

	JV = JobVars(pDir)
	HO = Fastq_Reader(JV.oDir)

	p2vecs = HO.oDir + HO.sdrTMP + 'vec/'

	while not os.path.isfile(p2vecs + 'stop'):
		FL = glob.glob(p2vecs  + '*.vec')
		if len(FL) < 2*HO.hSize:
			HO.generate_vector()
		
		time.sleep(int(JV.wts))
