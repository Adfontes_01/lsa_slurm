#!/usr/bin/env python

import sys,getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
import time
from job_vars import JobVars
from numpy import linalg as la

help_message = 'usage example: python kmer_vectors.py -p /project/home/ -n 8 -d'
if __name__ == "__main__":

	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')
	hashobject = StreamingEigenhashes(JV.oDir)


	saveDir = JV.oDir + JV.sdrCLS + 'VEC/'
	model= hashobject.load_decomp_model()

	for i in range(step):
		ind = step*fr + i
		if ind >= hashobject.nbr_C:
			break
		hash_gen = hashobject.kmer_chunk_from_disk(ind)
		vectors = hashobject.transform_vectors(model,hash_gen)
		tmp_mat = hashobject.vec2mat(vectors)

		fName = saveDir + extend_path(str(ind)) + '.npy'
		save_data(tmp_mat,fName,fl='npy')

