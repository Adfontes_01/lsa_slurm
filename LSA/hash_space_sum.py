#!/usr/bin/env python

import sys,getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from gensim import models
import time
from scipy.spatial import distance
import math
from job_vars import JobVars
from lsa_math import safe_cosine
import matplotlib.pyplot as plt
import random

help_message = 'usage example: python kmer_vectors.py -p /project/home/ -n 8 -d'
if __name__ == "__main__":

	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')
	HO = StreamingEigenhashes(JV.oDir)

	svDir = JV.oDir + JV.sdrCLS
	pdfDir = svDir + 'pdf/'
	blkDir = svDir + 'blk/'
	outDir = svDir + 'out/'

	
	print 'hash_max',HO.nbr_H
	print 'cMax',HO.cMax
	print 'HO.nbr_C',HO.nbr_C
	

	F =np.zeros(HO.nbr_H,dtype=np.float32)
	BLK = []
	OUT = []

	for k in xrange(HO.nbr_C):

		fName = pdfDir + extend_path(str(k)) + '.npy'
		if os.path.isfile(fName):
			tF = load_data(fName,fl='npy')
			F+=tF

		fName = blkDir + extend_path(str(k))+ '.blk'
		if os.path.isfile(fName):
			tB = load_data(fName,fl='list')
			BLK += tB

		fName = outDir + extend_path(str(k)) +'.out'
		if os.path.isfile(fName):
			tO = load_data(fName,fl='list')
			OUT += tO

	print len(F),len(BLK),len(OUT)
	
	fName = svDir + 'F.npy'
	save_data(F,fName,fl='npy')

	fName = svDir + 'BLK.blk'
	save_data(BLK,fName,fl='list')

	fName = svDir + 'OUT.out'
	save_data(OUT,fName,fl='list')

	C = [-1]*HO.nbr_H
	print 'BLK'
	for b in BLK:
		v,tI = b
		tI.sort()
		for i in tI:
			if C[i] != -1:
				v = C[i]
				break
		for i in tI:
			C[i] = v
		
	print 'OUT'
	for o in OUT:
		v,tI = o
		#print v, tI
		C[v] = v

	fName = svDir + 'C.c'
	save_data(C,fName,fl='list')
