#!/usr/bin/env python

import glob,os,time
import sys, getopt
import gzip,shutil
from fastq_reader import Fastq_Reader
from lsa_job import *
from lsa_math import kmer_bins
from counts_vector import Counts_Vector
from job_vars import JobVars


help_message = 'usage example: python hash_fastq_reads.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)

	do_reverse_compliment = True
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	FP = load_data(JV.oDir + JV.sdrJOB + '/fileList.txt',fl='str')
	AS,step = JV.get_array_size('files')
	HO = Fastq_Reader(JV.oDir)

	GH = Counts_Vector()
	GH.I = load_data(JV.oDir + JV.sdrCLS + 'hash_indices',fl='list')

	for i in range(step):
		ind = step*fr + i
		if ind >= HO.nbr_F:
			break

		file_name = get_file_name(FP[ind])
		file_name = cut_sufix_from_name(file_name,sfx='.fastq')
		smpDIR = get_prefix_from_name(file_name) + '/'

		hFile = JV.oDir + JV.sdrHASH + smpDIR + file_name + '.hashq.gz'
		print 'fr - ',fr,', i - ',i,', ind - ',ind, ', file_name_full ', hFile

		tFile = hFile+'.tmp'
		tf = open_file(tFile,arg='w')

		hGen = HO.read_from_hashq(hFile)
		for ls in hGen:

			tmp = []
			for v in ls:
				ii = GH.where(v)
				if ii < HO.nbr_H:
					if GH.I[ii]==v:
						tmp.append(ii)

			ss = ','.join([str(x) for x in tmp]) + '\n'
			tf.write(ss)

		tf.close()
		remove_file(hFile)
		
		with open_file(tFile, 'rb') as f_in, open_file(hFile, 'wb') as f_out:
			shutil.copyfileobj(f_in, f_out)
		remove_file(tFile)
