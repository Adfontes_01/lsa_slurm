#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars
from math import sqrt

help_message = 'usage example: python script_file.py -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h','--help'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg


        JV = JobVars(pDir)
	HO = StreamingEigenhashes(JV.oDir)

	JV.jobID = 'ClusterChunks'
	jname,oname,ename = JV.make_slurm_job()
	pid = run_slurm_job(jname)
	while check_slurm_pid(pid):
		print time.ctime(), JV.jobID, 'job is running, wait ' + str(HO.wts) + ' s'
		time.sleep(HO.wts)


	##################################################
	HO.load_clusters()

	CP = np.zeros(HO.nbr_Cls)

	save_dir = JV.oDir + JV.sdrCLS + HO.dec_cls_dir
	

	sf = 0
	for i in range(HO.nbr_C):
		lpath = save_dir + 'CLS/' + extend_path(str(i)) +'/'
		LS = load_data(lpath +'CC.list',fl='list')
		sf += LS[-1]
		tCP = load_data(lpath  + 'CP.npy',fl='npy')
		CP += tCP

	print HO.nbr_H,sf

	save_data(CP/CP.sum(),save_dir + 'cluster_probs.npy',fl='npy')

	
	CLS = np.memmap(save_dir + 'hash_cluster', dtype = np.uint16, mode='w+', shape = sf)
	bsCLS = 0

	PS  = np.memmap(save_dir + 'hash.pos',  dtype = np.uint64, mode='w+', shape = HO.nbr_H + 1)
	PS[0] = 0
	bsPS = 1

	for i in range(HO.nbr_C):
		print i,HO.nbr_C,PS[bsPS-1]
		lpath = save_dir + 'CLS/' + extend_path(str(i)) +'/'
		tCLS = load_data(lpath + 'hash_cluster.list',fl='list')
		tCLS = np.array(tCLS,dtype = np.uint16)
		CLS[bsCLS:(bsCLS+tCLS.size)] = tCLS
		bsCLS += tCLS.size

		tPS = load_data(lpath  + 'CC.list',fl='list')
		tPS = np.array(tPS[1:],dtype = np.uint64)
		tPS += PS[bsPS-1]
		PS[bsPS:(bsPS+tPS.size)] = tPS
		bsPS += tPS.size

	print bsPS,bsCLS
	print CLS
	print PS
	print CP

	del CLS,PS
	
	remove_dir(save_dir + 'CLS/')
