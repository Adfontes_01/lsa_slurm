#!/usr/bin/env python

import sys,getopt,os
from scipy.spatial import distance
import numpy as np
import math,time
from lsa_math import *
from lsa_job import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from lsa_stats import float_bins


help_message = "usage example: python define_files.py -o /project/home/"

def  save_hist(H,sDir,sta,mxb):

	nbr = len(H)
	xa = np.array([mxb*float(i)/nbr for i in range(nbr)])
	bar_width = mxb/(3*nbr)
	xax = [str(v) for v in xa]

	fig, ax = plt.subplots()
	rects1 = ax.bar(xa, H, bar_width, color='b')
	plt.xticks(xa[::3], xax[::3], rotation='vertical')
	ax.set_title(sta)
	plt.tight_layout()

	if not os.path.isdir(sDir):
		os.mkdir(sDir)
	
	fig.savefig(sDir + sta + '.png',format='png')
	plt.close(fig)


if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)


	STAL = ['data.rmUnmapped.sorted.bam2Stat','data.rmUnmapped.sorted.rmdup.bam2Stat','data.rmUnmapped.sorted.rmdup.bamFilter.bam2Stat','data.rmUnmapped.sorted.rmdup.bamFilter.uniq.bam2Stat']
	nbr = 100
	PFL = [0.0,0.01,0.05,0.1]
	MXB = [0.0001,0.001,0.01,0.1,0.25,0.5,1.0]
	CUT = [True,False]

	gp2fasta = '/env/cns/proj/ADAM/scratch/Results/EGAD_vdec/LSA_hyper_k30_h30_qcT_fuF/partitions/IDF_nr2_hw_vdec2_1000_IndMax/fastas/' 
	print gp2fasta
	NBC = load_data(gp2fasta + 'nbr_chanks.txt',fl='str')

	NBD = {}
	for el in NBC:
		nm = el[:el.find('/')]
		val = el[el.find(',')+1:]
		NBD[nm] = int(val) * 250000

	SCF = glob.glob(gp2fasta + '*/')

	for p2fasta in SCF:
		print p2fasta

		SL = glob.glob(p2fasta + 'EGAR00*')
		for sta in STAL:
			'''
			##########################
			R = ['sample,chunks,reads,bam2Stat,ratio']
			for sm in SL:
				#print sm
				sname = sm[(sm.rfind('/')+1):]
				#print sname
				p2file = sm+'/'+ sname+'/'+sta
				#print p2file
				#sys.exit()
				if os.path.isdir(sm+'/'+ sname):
					p2file = sm+'/'+ sname+'/'+sta
					if os.path.isfile(p2file):
						#print p2file
						#sys.exit()
						tx = load_data(p2file,fl='str')
						if (type(tx) is list) and len(tx) > 3:
							tx = tx[3]
							tx = tx[tx.rfind(':')+1:]
							tx = tx[:tx.find('(')]
							R.extend([sname+','+str(NBD[sname]/250000)+','+str(NBD[sname])+','+tx+','+str(float(tx)/NBD[sname])])
						elif (type(tx) is str) and len(tx) > 3:
							tx = tx[tx.rfind(':')+1:]
							R.extend([sname+','+str(NBD[sname]/250000)+','+str(NBD[sname])+','+tx+','+str(float(tx)/NBD[sname])])
						#else:
						#	print p2file
						#	print tx
						#	print type(tx),len(tx)
				#else:
				#	print 'del',sm
				#	os.system('rm -rf '+ sm)
			save_data(R,p2fasta+'PE_stats_' + sta + '.txt',fl='str')
			'''
			##########################
			txtFile = p2fasta+'PE_stats_' + sta + '.txt'
			print txtFile
			if not os.path.isfile(txtFile):
				continue

			TXT = load_data(txtFile,fl='str')

			V = np.zeros(len(TXT))
			for i in range(1,len(TXT)):
				ln=TXT[i]
				V[i] = float(ln[(ln.rfind(',')+1):])
			for pfl in PFL:
				for mxb in MXB:
					for cut in CUT:
						H = float_bins(V,nbr,p_filter = pfl, mxb=mxb,cut=cut)
						sDir = p2fasta+'PE_hist_pfl_'+str(pfl)+'_mxb_'+str(mxb)+'_cut_'+str(cut)+'/'
						#print sDir
						save_hist(H,sDir,sta,mxb)

			'''
			fig, ax = plt.subplots()
			#ax.stem(range(len(H)), H)
			rects1 = ax.bar(xa, H, bar_width, color='b')
			plt.xticks(xa[::3], xax[::3], rotation='vertical')
			ax.set_title(sta)

			sDir = p2fasta+'PE_hist_'+str(mxv)+'/'
			if not os.path.isdir(sDir):
				os.mkdir(sDir)

			fig.savefig(sDir + sta + '.png',format='png')
			plt.close(fig)
			'''
			
