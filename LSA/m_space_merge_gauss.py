#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars
from collections import defaultdict
from space_m import  mat_count_connects,uv_un_D, merge_sparse,threshold_connects,gauss_mask_dists,gauss_merge_connected
from shutil import copyfile
		
help_message = 'usage example: python script.py -r 0 -p /project/home/'

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in '-h':
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')

	HO = StreamingEigenhashes(JV.oDir)
	model = HO.load_decomp_model()

	HO.open_AM_files()

	JV.jobID = HO.clsType+'CountConnects'
	jname,oname,ename = JV.make_slurm_job()
	scriptName = jname[(jname.rfind('/')+1):]
	pid = run_slurm_job(jname)
	wait_for_slurm_jobs(pid,int(JV.wts),job = scriptName)

	read_dir = HO.oDir + HO.sdrTMP + HO.dec_cls_dir
	X = [load_data(read_dir + 'X_'+str(i),fl='mm',dump=np.float32) for i in range(HO.nbr_T)]
	VI = np.memmap(read_dir+'VI', dtype = np.uint64, mode='r+', shape=(HO.nbr_H,))
	nbr = HO.ms_m()

	uv,un,D = uv_un_D(VI)
	cl = uv.size
	print 'cls',cl

	FL = get_file_list(PATH=read_dir+'tCN/',END='*cn')
	CN =  merge_sparse(FL,cl)

	CN = threshold_connects(CN,HO.nbr_T)
	print len(CN)

	CN = gauss_mask_dists(X,VI,CN,mS_th_ds=int(JV.mS_th_ds))
	print len(CN)

	print 'gauss merge clusters'
	gauss_merge_connected(X,VI,CN)
	uv,un,D = uv_un_D(VI)
	print 'final nbr',uv.size,un
	print 'sorted',np.sort(un)

	HO.close_AM_files()		

	savedir = JV.oDir + JV.sdrCLS + HO.dec_cls_dir

	save_data(uv.size, savedir+'nbrClusters_gauss.txt',fl='nbr')
	save_data(uv.size, savedir+'nbrClusters.txt',fl='nbr')
	copyfile(read_dir+'VI', read_dir+'VI'+'_gauss')

	del VI
