import numpy as np
import sys,math
from scipy.spatial import distance

def size_m_1dim(d,n):
	m = math.sqrt(n)
	k = int((n**((d-1.0)/d))*(m**(1.0/d)))

	if (k == n) and (k > 1):
		k -= 1

	return k

def get_neighbors(V,INDX,J,m):
	
	d = len(INDX)
	n = len(INDX[0])

	IN = []
	k = m
	while len(IN) < (m+1):
		T = [[] for i in range(d)]
		for i in range(d):
			lb = J[i]-k
			if lb < 0:
				lb = 0
			ub = J[i]+k
			if ub > n:
				ub = n
			tI = INDX[i][lb:ub]
			T[i] = tI
		UI = [j for i in T for j in i]
		UI = list(set(UI))

		IN = set(T[0])
		for i in range(1,d):
			IN = IN.intersection(T[i])
		IN = list(IN)
		k *= 2
	UI.sort()
	IN.sort()

	return IN,UI,T

# PAIRED READ FILES ARE ASSUMED TO BE SORTED
def kmer_bins(A,B):
	
	if A is None:
		return []

	current_id = None
	bins = []
	reads_hashed = 0
	HSH=[]
	for i in range(len(A)):
		if A[i] != current_id:
			if bins:
				HSH.append(bins)
				reads_hashed += 1
				bins = []
			current_id = A[i]
		bins.extend([int(v) for v in  B[i,:]])

	if bins:
		HSH.append(bins)
		reads_hashed += 1
		bins = []

	return HSH

def  safe_cosine(base_block,block,thr = 1.0):
      
      np.seterr(divide='ignore', invalid='ignore')
      tD = distance.cdist(base_block,block,'cosine')
      eps = np.finfo(tD.dtype).eps

      tD[np.isnan(tD)] = 2.0
      tD[tD > thr] = 2.0
      tD[tD < eps] = 0.0
      return tD

def lsq_nn(C,d,tol):
        n = len(d)
        P = np.zeros(n,dtype=bool)
        x = np.zeros(n,dtype=np.float32)
        z = np.zeros(n,dtype=np.float32)

        r = d - np.dot(C,x)
        w = np.dot(r,C)
        w[w<0.0]=0.0

        it = 0
        itmax = 3*n
        while any(np.logical_not(P)) and any(w[np.logical_not(P)] > tol):
              z = np.zeros(n,dtype=np.float32)

              t = np.where(w[np.logical_not(P)].max() == w)[0][0]
              P[t] = True
              z[P]=np.dot(np.linalg.pinv(C[:,P]),d)
              while any(z[P]<=0.0):
                    it += 1
                    if it > itmax:
                          z[z<0.0] = 0.0
                          return z
                    Q = np.logical_and(z <= 0.0,P)
                    alpha = (x[Q]/(x[Q]-z[Q])).min()
                    x += alpha*(z-x)
                    P = np.logical_and(abs(x)>tol,P)
                    z = np.zeros(n,dtype=np.float32)

                    z[P]=np.dot(np.linalg.pinv(C[:,P]),d)
              x = z
              r = d - np.dot(C,x)
              w = np.dot(r,C)
              w[w<0.0]=0.0
        
        z[z<0.0] = 0.0
        return z

