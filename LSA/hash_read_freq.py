#!/usr/bin/env python

import glob,os
import sys, getopt
import gzip,time
from fastq_reader import Fastq_Reader
from lsa_job import *
from lsa_math import kmer_bins
from job_vars import JobVars
from collections import defaultdict
from scipy.sparse import dok_matrix,vstack

help_message = 'usage example: python hash_fastq_reads.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg


        JV = JobVars(pDir)
	FP = load_data(JV.oDir + JV.sdrJOB + '/fileList.txt',fl='str')

	AS,step = JV.get_array_size('files')
	HO = Fastq_Reader(JV.oDir)

	print 'start wheel loading'
	HO.load_wheel()
	dvd = 2 if HO.paired_reads else 1
	
	nbr_rf = HO.fReads/dvd
	print nbr_rf,len(FP),len(FP)*nbr_rf,HO.nbr_H
	gCN = dok_matrix((len(FP)*nbr_rf,HO.nbr_H), dtype=bool)

	for i in range(step):
		ind = step*fr + i
		if ind >= len(FP):
			break

		file_name = get_file_name(FP[ind])
		file_name = cut_sufix_from_name(file_name,sfx='.fastq')
		smpDIR = get_prefix_from_name(file_name) + '/'

		hFile = JV.oDir + JV.sdrHASH + smpDIR + file_name + '.hashq.gz'
		print 'fr - ',fr,', i - ',i,', ind - ',ind, ', file_name -  ',file_name

		HRG = HO.read_from_hashq(hFile)

		rBS = ind*nbr_rf

		r_id = 0
		for vec in HRG:
			for el in vec:
				gCN[rBS+r_id/dvd,el] = True
			r_id += 1

		sh = gCN.shape
		print r_id,sh,gCN.getnnz(),float(gCN.getnnz())/((r_id/dvd)*sh[1])

	if gCN.getnnz():
		D = dok2links(gCN)
		save_data(D,JV.oDir + JV.sdrTMP + 'hLinks/CN/'+ str(fr) + '.cn.gz',fl='list',dump='pk')
