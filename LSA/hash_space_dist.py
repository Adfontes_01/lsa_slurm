#!/usr/bin/env python

import sys,getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from gensim import models
import time
from scipy.spatial import distance
import math
from job_vars import JobVars
from lsa_math import safe_cosine
import matplotlib.pyplot as plt
import random

help_message = 'usage example: python kmer_vectors.py -p /project/home/ -n 8 -d'
if __name__ == "__main__":

	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')
	HO = StreamingEigenhashes(JV.oDir)

	pdfDir = JV.oDir + JV.sdrCLS + 'pdf/'
	blkDir = JV.oDir + JV.sdrCLS + 'blk/'
	outDir = JV.oDir + JV.sdrCLS + 'out/'

	
	m = int(math.sqrt(HO.nbr_H))
	print 'hash_max',HO.nbr_H

	print 'cMax',HO.cMax
	print 'HO.nbr_C',HO.nbr_C

	model= HO.load_decomp_model()
	print 'HO.nbr_T',HO.nbr_T


	HASH = np.zeros((0,2))
	for i in range(HASH.shape[1]):
		t=2*np.random.rand(5,2)-1
		t[:,i] += 10
		HASH = np.vstack((HASH,t))
	print HASH.shape
	print HASH
	
	HO.nbr_T = HASH.shape[1]
	HO.nbr_H = HASH.shape[0]
	fName = JV.oDir + JV.sdrCLS + 'tmp.npy'
	save_data(HASH,fName,fl='npy')

	print str(fr) + ', HASH was created'

	m = int(math.sqrt(HO.nbr_H))
	print 'm',m
	F = np.zeros(HO.nbr_H,dtype=np.float32)
	vc= np.zeros((1,HO.nbr_T),dtype=np.float32)
	eps = np.finfo(F.dtype).eps
	BL = []
	OU = []
	
	kk = 0
	ll = 0
	print
	for k in xrange(HO.nbr_H):
		print '---------------'
		print k,'-',HO.nbr_H,kk,ll

		vc[0,:] = HASH[k,:]
		tD = safe_cosine(vc, HASH, HO.clThresh)[0]
		print tD
		
		sI = np.argsort(tD)
		tD = tD[sI]
		print tD

		if tD[-1] == 2:
			n2 = sum(tD == 2)
			sI = sI[:(-n2)]
			tD = tD[:(-n2)]
			if sI.size <= m:
				OU += [(k,[int(val) for val in sI])]
				kk += 1
				continue
				
		if tD[1] == 0:
			nz = sum(tD == 0)
			BL += [(k,[int(val) for val in sI[:nz]])]
			ll += 1
		sg = 1*float(tD[m])
		print sg
		sg += eps

		print sg

		print np.exp(-(0.5*(tD/sg)**2))
		tF = ((1.0/sg)*np.exp(-(0.5*(tD/sg)**2)))
		print sI
		print tF
		F[sI] += tF
	print
	print F
	print 'BL-',len(BL),', OU-',len(OU)
	F /= np.max(F)
 
	fName = pdfDir + extend_path(str(fr)) + '.npy'
	save_data(F,fName,fl='npy')

	fName = blkDir + extend_path(str(fr))+ '.blk'
	save_data(BL,fName,fl='list')

	fName = outDir + extend_path(str(fr)) +'.out'
	save_data(OU,fName,fl='list')

