import gzip,sys,glob,os,shutil,signal,time
import fnmatch
import cPickle as pk
import marshal as mr
import numpy as np
from lsa_names import *
from scipy.spatial import distance
from scipy.sparse import dok_matrix
from collections import defaultdict

def check_system():
	if sys.platform[:3] == 'win':
		raise Exception("Sorry, it does not work on Windows")

	if sys.version_info.major == 3:
		raise Exception("Sorry, must be using Python 2")

	slurm = os.popen('sinfo 2>&1').read().splitlines()
	if len(slurm)<2:
		raise Exception('There is no SLURM system or partition installed')

def run_slurm_job(sName):

	if not os.path.isfile(sName):
		print 'Error: check ' + sName +  ' file'
		raise NameError(sName + ' job file was not created')
	print time.ctime(),'running ' + sName[sName.rfind('/'):] + ' job via sbatch'
	
	resp = os.popen('sbatch '+ sName).read()
	if 'error' in resp:
		print sName + ' job was not started'
		raise NameError(resps)
	try:
		pid = resp.split()[3]
	except:
		raise NameError(resp.split())

	return pid

def open_file(fname,arg='r'):

      dr = get_dir_name(fname)
      if (not os.path.isdir(dr)) and (len(dr)>0) and ('r' not in arg):
            os.system('mkdir -p ' + dr)
      tn = 0
      next = True
      while next and (tn < 10):
	      try:
		      if fname[-2:] == 'gz':
			      f = gzip.open(fname,arg)
		      else:
			      f = open(fname,arg)
		      next = False
	      except:
		      tn += 1 
		      time.sleep(1)

      if next:
	      raise NameError('cannot open file ' + fname)

      return f

def free_space(path):
	sos = os.statvfs(path)
	freeSZ = sos.f_frsize * sos.f_bavail
	return freeSZ


def file_size(fname):
	sz = -1
	if os.path.isfile(fname):
		if fname[-2:]=='gz':
			sz = int(os.popen('gunzip -l '+ fname).read().split()[5])
		else:
			sz = int(os.path.getsize(fname))
	elif os.path.isdir(fname):
		FL = get_file_list(PATH=fname,PFX='*')
		sz = 0
		for fl in FL:
			sz += file_size(fl)
	return sz

def extend_path(ps,nb = 3):

      rs = len(ps) - nb
      if rs > 0:
            ps = '/'.join(list(ps[:rs]))+'/'+ps[rs:]

      return ps
	
def load_csv_lines(fname):
	data = {}
	f = open_file(fname)
	line = 'empty'
	while len(line):
		line = f.readline()
		ps = line.find(',')
		if ps == -1:
			continue
		spl = [line[:ps].strip(), line[(ps+1):].strip()]
		if spl[0][0] == '#':
			continue
		if (spl[1][0] =='\'') | (spl[1][0] =='\"'):
			spl[1] = spl[1][1:-1]
		data[spl[0]] = spl[1]
	f.close()
	return data


def load_data(fname, fl=None, dump=None):

      if fl == 'mm':
	      F = np.memmap(fname, dtype = dump, mode='r')
	      return F

      if fl == 'npy':
	      i = 0
	      while True:
		      try:
			      LS = np.load(fname)
			      return LS
		      except:
			      time.sleep(1)
			      i += 1
			      if i == 10:
				      raise NameError('cannot open file ' + fname)

      if fl == 'npz':
	      LS = np.load(fname)
	      LS = LS['LS']
	      return LS

      if (fl == 'job') or (fl == 'prj'):
	      if fname[-1] == '/':
		      fname += 'JobVars'

	      JobVars = load_csv_lines(fname)
	      JobVars = check_job_vars(JobVars)

	      return JobVars

      f = open_file(fname,'rb')

      if (fl == 'list') or (fl is None):
	      if (fname[-3:] == '.gz') or (dump =='pk'):
		      LS = pk.load(f)
	      else:
		      LS = mr.load(f)
      elif fl == 'str':
	      LS = f.read().splitlines()
      elif fl == 'nbr':
	      LS = f.read().splitlines()
	      LS = [int(v) for v in LS]
      else:
	      f.close()
	      raise ValueError('data type is not specified')

      f.close()
      if len(LS) == 1:
	      LS = LS[0]

      return LS

def make_dir(dr):

	if (len(dr)>0):
		if (not os.path.isdir(dr)):
			os.system('mkdir -p ' + dr)
			i = 0
			while not os.path.isdir(dr):
				time.sleep(1)
				i += 1
				if i == 10:
					raise NameError('cannot create dir ' + dr)


def save_data(LS, fname, fl=None, dump=None):

      if fl is not None:
	      dr = get_dir_name(fname)
	      make_dir(dr)
	      #print 'dr',dr

      if fl == 'mm':
	      F = np.memmap(fname, dtype = LS.dtype, mode='w+', shape=LS.shape)
	      F[:] = LS
	      del F
	      return

      if fl == 'npy':
	      np.save(fname,LS)
	      return
      if fl == 'npz':
	      np.savez_compressed(fname,LS=LS)
	      return

      if fl == 'job':
	      fname += 'JobVars'
	      if os.path.isfile(fname):
		      tmp = load_data(LS['oDir'],fl='job')
		      for key, val in tmp.items():
			      if not key in LS:
				      LS[key]=val
	      f = open_file(fname, "w")
	      for key, val in LS.items():
		      f.write(key + ',' + val +'\n')
	      f.close()
	      return

      f = open_file(fname, 'wb')
      if type(LS) != list:
	      LS=[LS]

      if (fl == 'list') or (fl is None):
	      if (fname[-3:] == '.gz') or (dump =='pk'):
		      pk.dump(LS, f,protocol = -1)
	      else:
		      mr.dump(LS, f)
      elif fl == 'str':
	      f.write("\n".join(LS) + '\n')
      elif fl == 'nbr':
	      LS = [str(v) for v in LS]
	      f.write("\n".join(LS) + '\n')
      else:
	      f.close()
	      raise ValueError('data type is not specified')

      f.close()

def check_job_vars(JobVars):

	Chk = load_csv_lines(JobVars['cDir'] + 'SLURM/default.vals')

	if not 'oDir' in JobVars.keys():
		Chk['oDir'] = JobVars['iDir']

	if not 'verbose' in JobVars.keys():
		JobVars['verbose'] = Chk['verbose']

	for key,val in Chk.items():
		if not key in JobVars:
			if JobVars['verbose'] == 'True':
				print '------------------------------------------------'
				print '"' + key + '" is not defined into the initial project file'
				print 'default value:',val
			JobVars[key]=val
	JobVars = check_slash(JobVars)
	JobVars['sPart'] = JobVars['sPart'].split(',')[0]
      
	return JobVars

def str2val(s):
	if type(s) is not str:
		return s

	tmp = str.upper(s)
	if tmp in ['TRUE','FALSE']:
		val = tmp == 'TRUE'
		return val

	val = s
	try:
		val = int(s)
		return val
	except:
		pass
	try:
		val = float(s)
		return val
	except:
		pass

	return val

		

def check_slash(JobVars):
      for key, val in JobVars.items():
            if ('Dir' in key) or ('sdr' in key):
                  if val[-1] != '/':
                        val += '/'
                        JobVars[key]=val
      return JobVars

def str_rep_dict(tmp,obj):
	dc = obj
	if type(dc) is not dict:
		dc = dc.__dict__
	for key in dc.keys():
              tmp = tmp.replace(key,dc[key])
	return tmp

def rep_in_str(text,obj):

	fl = False
	if type(text) is not list:
		fl = True
		text = [text]

	for i in range(len(text)):
		text[i] = str_rep_dict(text[i],obj)
	if fl:
		text = text[0]
	return text

def remove_dir(dr):
	if os.path.isdir(dr):
		pids=os.popen('lsof -t +D '+ dr).read().split()
		for pid in pids:
			#os.kill(int(pid), signal.SIGTERM) #or signal.SIGKILL 
			os.system('kill -9 ' + pid)
			time.sleep(1)
		shutil.rmtree(dr)
		print dr,'was removed'

def remove_file(fName):
	if os.path.isfile(fName):
		os.remove(fName)

def remove_files(dr,fMask):
	filelist = glob.glob(dr + fMask)
	for f in filelist:
		if os.path.isfile(f):
			os.remove(f)

def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

def nbr_lines_file(file_name):

	f = open_file(file_name)
	r = 0
	line = None
	while (line != ''):
		line = f.readline()
		if line:
			r += 1
	f.close()

	return r


def read_len(f):
	nr=100
	last = f.tell()
	L = [f.readline().rstrip() for _ in range(4*nr)]
	L=L[1::4]
	f.seek(last)
	LN = [len(l) for l in L]
	LN.sort()
	read_len = LN[len(LN)/2]
	return read_len

def paired_reads(f):
	initial_pos = f.tell()
	L = [f.readline() for _ in range(8)]
	f.seek(initial_pos)
	L = L[::4]
	L = [l.strip().split()[0] for l in L]
	L = [l[l.find('.')+1:] for l in L]
	for i in range(len(L)):
		ind = L[i].rfind('.')
		if ind == -1:
			ind = L[i].rfind('/')
		if ind != -1:
			L[i]=L[i][:ind]

	paired_reads = (L[0] == L[1]) 
	return paired_reads

def get_quality_code(f):
	print 'get_quality_code'
        nr=1000
        last = f.tell()
        L = [f.readline() for _ in range(4*nr)]
        f.seek(last)
        QC = 64
        sm = 0
        for l in L[3::4]:
                for c in l.strip():
                        if ord(c) < 64:
                                sm += 1
                if sm > 10:
                      QC = 33
                      break

        return QC

def str_diff(str1,str2):
	ln = max(len(str1),len(str2))
	df1=''
	df2=''
	for i in range(ln):
		lt1 = str1[i:i+1]
		lt2 = str2[i:i+1]
		if lt1 != lt2:
			df1 += lt1
			df2 += lt2
	return df1,df2

def str_comm_head(str1,str2):
	ln = min(len(str1),len(str2))
	cstr=''
	for i in range(ln):
		lt1 = str1[i]
		lt2 = str2[i]
		if lt1 == lt2:
			cstr += lt1
		else:
			break
	return cstr

def get_file_list(PATH='',PFX='',SFX='',END='',PFX_dir = False):
	lFiles=[]
	PATH_len = len(PATH)
	PFX_len = len(PFX)
	for dr,drnames,fnames in os.walk(PATH):

		if PFX_dir:
			dir_end = dr[PATH_len:]
			dir_spl = dir_end.split('/')
			if not all([val[:PFX_len] == PFX for val in dir_spl]) and (len(dir_end)>0):
				continue

		mnames = fnmatch.filter(fnames,PFX+SFX+END)
		for fn in mnames:
			lFiles.append(dr+'/'+fn)
	if len(lFiles) == 0:
		raise NameError('we did not find any files in:\n'+ PATH +'\nchange input parameters')
	return lFiles

def get_slurm_pids():
      PIDs=os.popen('squeue | grep '+ os.getenv('USER')).read().split()
      PIDs=PIDs[::8]
      return PIDs


def check_slurm_pid(pd):
	
	PIDs = get_slurm_pids()
	
	if type(pd) is not list:
		pd = [pd]
	#print 'pd,PIDS',pd,PIDs
	res = []
	for p in pd:
		if any(p in pp for pp in PIDs):
			res.append(p)
	#print 'res',res
	return res

def wait_for_slurm_jobs(pd,wts,job = ''):

	while check_slurm_pid(pd):
		print time.ctime(), job,pd, 'job is running, wait ' + str(wts) + ' s'
		time.sleep(wts)
	print time.ctime(),job,pd,'job is finished'



def convert_compress_pairs(outputdir,sname,fname,f1,f2,mrg):

      ch = 0

      fnm = file_name_merged(outputdir,sname,fname,str(ch))
      f0 = open_file(fnm,'w')
      reads_written = 0
      bytes_written = 0
      lrw = 0
      lbw = 0
      while True:
            r1 = []
            r2 = []
            try:
                  r1 = [f1.readline() for _ in range(4)]
                  if f2 is None:
                        r2 = []
                  else:
                        r2 = [f2.readline() for _ in range(4)]
            except:
                  pass
	    
	    sm1 = sum(len(s) for s in r1)
	    sm2 = sum(len(s) for s in r2)
	    if (f2 is None) and (sm1 == 0):
		    break
	    if (f2 is not None) and ((sm1 == 0) or (sm2==0)):
		    break

            f0.writelines(r1)
            lrw += 1
            if (f2 != None):
                  f0.writelines(r2)
                  lrw += 1
            lbw += (sm1 + sm2)

            if lrw >= mrg:
                  print 'lrw - ', lrw, ' lbw - ',lbw
                  reads_written += lrw
                  bytes_written += lbw
                  lrw = 0
                  lbw = 0
                  f0.close()
                  ch += 1
                  fnm = file_name_merged(outputdir,sname,fname,str(ch))
                  f0 = open_file(fnm,'w')
      
      f0.close()
      
      if lrw == 0:
	      os.remove(fnm)
      else:
            reads_written += lrw
            bytes_written += lbw
            ch += 1

      return ch,reads_written,bytes_written

def dok2dict(CN):
	D={}
	for k,v in CN.items():
		D[(int(k[0]),int(k[1]))] = int(v)
	return D

def dok2links(CN):
	D = defaultdict(list)
	for k,v in CN.iteritems():
		D[int(k[1])].append(int(k[0]))
	return D

def dok_sum(CN,dm):
	D = defaultdict(int)
	for k,v in CN.iteritems():
		D[int(k[dm])] += int(v)
	return D
