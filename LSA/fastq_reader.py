from random import randint
import glob,os,sys
import numpy as np
from lsa_base import LSA
from hyper_sequences import Hyper_Sequences
from hash_counting import Hash_Counting
from lsa_job import load_data,load_data, open_file, file_size, str2val
from job_vars import JobVars
from lsa_names import get_file_name

class Fastq_Reader(Hash_Counting,Hyper_Sequences,LSA):

	def __init__(self,oDir):
		super(Fastq_Reader,self).__init__(oDir)
		
		JV = JobVars(oDir)
		self.hMax = JV.hash_max()
		self.clone(JV)
		self.init_lsa()

		del JV

	def bipart_fa(self,fname):

		name = get_file_name(fname)
		nm1 = self.oDir + self.sdrTMP + name +'1'
		nm2 = self.oDir + self.sdrTMP + name +'2'

		fo1 = open_file(nm1,'w')
		fo2 = open_file(nm2,'w')
		for read in self.read_from_fa(fname):
			ln0 = read[0][1:-1]
			ln0 = ln0[:ln0.find('haplotype_infix')-1]
			ln1 = read[1][:-1]
			wrt = ln0 + '\n' + ln1 + '\n'

			if '/1' in ln0:
				fo1.write(wrt)
			else:
				fo2.write(wrt)

		fo1.close()
		fo2.close()

	def split_samples_fastq(self,ind):

		FP = load_data(self.oDir + self.sdrJOB + '/fileList.txt',fl='str')
		if type(FP) is not list:
			FP = [FP]
		fname = FP[ind]

		file_object = open_file(fname,'r')
		DS = {}

		for read in self.read_from_fastq(file_object,paired_readse=False,nbr_reads=1):
			
			read_block = read.split()
			read_sample = read_block[0]
			read_sample = read_sample[1:read_sample.find('.')]

			path2sample = self.oDir + self.sdrSAMPLES + read_sample + '/'
			if not os.path.isdir(path2sample):
				os.system('mkdir -p ' + path2sample)
			path2sample_file =  path2sample + read_sample + '.' + str(ind) + '.fastq.gz'

			if read_sample not in DS.keys():
				print path2sample_file
				DS[read_sample] = open_file(path2sample_file, 'w')
			DS[read_sample].write(read)

		for fl in DS.keys():
			DS[fl].close()

		file_object.close()

	def read_generator(self,file_object, max_reads=1,verbose_ids=False,raw_reads=False, check_qc=True):
		self.set_quality_dict(file_object)
		
		line = 'dummyline'
		r = 0
		while (line != '') and (r < max_reads):
			line = file_object.readline().strip()
			if line:
				if line[0] == '@':
					try:
						READ = line + '\n'
						I = line
						line = file_object.readline().strip()
						READ += (line + '\n')
						S = line

						S = S.upper()
						line = file_object.readline().strip()
						READ += (line + '\n')
						line = file_object.readline().strip()
						if len(line) != len(S):
							continue
						READ += (line + '\n')

						if raw_reads:
							yield READ
						else:
							if verbose_ids:
								I = READ

							Q = [self.quality_codes[c] for c in line]
							if (S) and (Q):
								i = len(S)
								if check_qc:
									low_qual = 0
									i = -1
									while (i < len(S)-self.kMer) and (low_qual < 3):
										if Q[i+self.kMer] < 3:
											low_qual += 1
										i += 1
								yield {'_id': I,'s': S[:i+self.kMer],'q': Q[:i+self.kMer]}
						r += 1
					except Exception,err:
						return

	

	def read_from_hashq(self,file_name):
		f = open_file(file_name)

		line = None
		while (line != ''):
			line = f.readline()
			if line:
				ls = line[:-1]
				ls = ls.split(',')
				vals = [int(x) for x in ls]
				yield vals
		f.close()


	def read_from_fa(self,file_name):
		f = open_file(file_name)
		line1 = None
		while (line1 != ''):
			line1 = f.readline()
			if line1:
				line2 = f.readline()
				yield line1,line2
		f.close()

	def read_from_fastq(self,file_object,paired_reads=False,nbr_reads=None):

		line = None
		cont = True
		r = 0
		while (line != '') and (cont):
			line = file_object.readline()
			if line:
				if line[0] == '@':
					verbose_id = line
					verbose_id += file_object.readline()
					verbose_id += file_object.readline()
					verbose_id += file_object.readline()
					if paired_reads:
						verbose_id += file_object.readline()
						verbose_id += file_object.readline()
						verbose_id += file_object.readline()
						verbose_id += file_object.readline()
					r += 1
					yield verbose_id

			if nbr_reads:
				if r == nbr_reads:
					cont = False

	def set_quality_dict(self,f):

		if self.QC is None:
			from lsa_job import get_quality_code
			self.QC = get_quality_code(f)
		if hasattr(self, 'quality_codes'):
			return
		if (self.QC == 33):
			self.quality_codes = dict([(chr(x),x-33) for x in range(33,33+94)])
			return
		if (self.QC == 64):
			self.quality_codes = dict([(chr(x),x-64) for x in range(64-5,64+63)])
			return

	def rand_kmers_for_wheel(self, p2rkm, nbr_vec = None):

		if nbr_vec is None:
			total_kmers = self.kMer*self.hSize
		else:
			total_kmers = self.kMer

		RP = load_data(self.oDir + self.sdrJOB + 'fileList.txt',fl='str')
		g = open_file(p2rkm,'w')

		for i in xrange(total_kmers):
			rp = RP[randint(0,self.nbr_F-1)]
			f = open_file(rp)
			max_seek = file_size(rp)
			g.write(self.rand_kmer(f,max_seek))
			f.close()
		g.close()

	def rand_kmer(self,f,max_seek=None):
		if max_seek is None:
			max_seek = file_size(str(f).split("'")[1])

		while True:
			f.seek(randint(0,max_seek-1))
			rs = [_ for _ in self.read_generator(f,max_reads=1,verbose_ids=True)]
			if len(rs) > 0:
				if len(rs[0]['s']) > self.kMer:
					break
		ri = randint(0,len(rs[0]['s'])-self.kMer)
		rs = rs[0]['_id'].split('\n')
		return '\n'.join([rs[0],rs[1][ri:ri+self.kMer],rs[2],rs[3][ri:ri+self.kMer]+'\n']) 
