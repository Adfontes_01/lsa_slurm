#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from lsa_job import *
from job_vars import JobVars
import yaml
from collections import defaultdict
#from sklearn.metrics import homogeneity_completeness_v_measure


help_message = 'usage example: python kmer_cluster_merge.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)

	numCLS = load_data(JV.oDir + JV.sdrCLS + JV.dec_cls_dir() + 'nbrClusters.txt',fl='nbr') 
	print 'numCLS',numCLS
	listSMP = load_data(JV.oDir + JV.sdrJOB + 'sampleList.txt',fl='str') 
	print 'numSMP',len(listSMP)

	FL = get_file_list(PATH=JV.oDir + JV.sdrREP + 'reads2class/' + JV.dec_cls_dir()+'jobs/',PFX='job',END='*.sts')
	CNrd = np.zeros((int(JV.nbr_S),numCLS),dtype=np.uint64)
	i = 0
	for fl in FL:
		print i,len(FL)
		i += 1
		sts = load_data(fl,fl='str') 
		if type(sts) is not list:
			sts = [sts]
		for line in sts:
			ps_cm = line.find(',')
			ps_pn = line.find('.')
			ps_name = listSMP.index(line[:ps_pn])
			nb =[int(val) for val in line[(ps_cm+1):].split(',')]
			for j in range(len(nb)):
				CNrd[ps_name,j] += nb[j]

	save_data(int(np.sum(np.sum(CNrd,axis=0)!=0)),JV.oDir + JV.sdrREP + 'reads2class/' + JV.dec_cls_dir()+'nbrParts.txt',fl='nbr')	
	prt_rd = np.sum(CNrd,axis=0)
	smp_rd = np.sum(CNrd,axis=1)
	prt_smp = np.sum(CNrd!=0,axis=0)
	smp2prt = np.sum(CNrd!=0,axis=1)

	ST = listSMP[:]
	for i in range(CNrd.shape[0]):
		ST[i] += (','+ ','.join(map(str,CNrd[i,:])))
	save_data(ST,JV.oDir + JV.sdrREP + 'reads2class/' + JV.dec_cls_dir()+'table.txt',fl='str')

	ST = listSMP[:]
	for i in range(CNrd.shape[0]):
		ln = CNrd[i,:].astype(np.float)
		ln /= np.sum(ln)
		ST[i] += (','+ ','.join(map(str,ln)))
	save_data(ST,JV.oDir + JV.sdrREP + 'reads2class/' + JV.dec_cls_dir()+'table_samples_rt.txt',fl='str')

	ST=['i,smp,#reads,#parts,entrp']
	STI=[]
	for i in range(CNrd.shape[0]):
		nzi = np.nonzero(CNrd[i,:])[0]
		vec = CNrd[i,nzi].astype(np.float)
		vec /= vec.sum()
		nbs = vec.size
		entrp = -sum(vec*np.log(vec)/np.log(nbs)) if nbs > 1 else 0.0
		rc =[i,listSMP[i],int(np.sum(CNrd[i,:],axis=0)),len(nzi),entrp]
		ST.append(','.join(map(str,rc)))
		rc = [i,listSMP[i]]
		rc.extend([v for v in nzi])
		STI.append(','.join(map(str,rc)))

	save_data(ST,JV.oDir + JV.sdrREP + 'reads2class/' + JV.dec_cls_dir()+'samples_reads_parts.txt',fl='str')
	save_data(STI,JV.oDir + JV.sdrREP + 'reads2class/' + JV.dec_cls_dir()+'samples_parts_ind.txt',fl='str')

	ST=['#prt,#reads,#samples,entrp']
	STI=[]
	for j in range(CNrd.shape[1]):
		nzi = np.nonzero(CNrd[:,j])[0]
		vec = CNrd[nzi,j].astype(np.float)
		vec /= vec.sum()
		nbs = vec.size
		entrp = -sum(vec*np.log(vec)/np.log(nbs)) if nbs > 1 else 0.0
		rc =[j,int(np.sum(CNrd[:,j],axis=0)),len(nzi),entrp]
		ST.append(','.join(map(str,rc)))
		rc = [j]
		rc.extend([v for v in nzi])
		STI.append(','.join(map(str,rc)))

	save_data(ST,JV.oDir + JV.sdrREP + 'reads2class/' + JV.dec_cls_dir()+'patrs_reads_samples.txt',fl='str')
	save_data(STI,JV.oDir + JV.sdrREP + 'reads2class/' + JV.dec_cls_dir()+'parts_samples_ind.txt',fl='str')
	print 'stats is saved into',JV.oDir + JV.sdrREP + 'reads2class/' + JV.dec_cls_dir() 
