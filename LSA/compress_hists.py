#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from fastq_reader import Fastq_Reader
from lsa_job import *
from lsa_stats import SafeRt,get_counts
from counts_vector import Counts_Vector
from job_vars import JobVars
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

help_message = 'usage example: python kmer_corpus.py -r 0 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h','--help'):
			print help_message
			sys.exit()
		elif opt in ('-r','--filerank'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	FP = load_data(JV.oDir + JV.sdrJOB + '/sampleList.txt',fl='str')
	AS,step = JV.get_array_size('samples')
	HO = Fastq_Reader(JV.oDir)

	GH = Counts_Vector()
	GH.I = load_data(JV.oDir + JV.sdrCLS + 'hash_indices',fl='list')
	hmax = JV.hash_max()
		
	path2save = JV.oDir + JV.sdrREP + 'hash/'

	for i in range(step):
		ind = step*fr + i
		if ind >= len(FP):
			break
		sample_prefix = FP[ind]
		print sample_prefix

		fname = JV.oDir + JV.sdrHASH + sample_prefix + '.count.hash.nzi'
		nzi = load_data(fname,fl = 'list')

		fnameH = JV.oDir + JV.sdrHASH + sample_prefix + '.count.hash'
		HS = load_data(fnameH,fl = 'mm',dump=np.uint16)


		tmp = []
		tmpH = []
		for i in xrange(len(nzi)):
			v = nzi[i]
			ii = GH.where(v)
			if ii < HO.nbr_H:
				if GH.I[ii]==v:
					tmp.append(ii)
					tmpH.append(int(HS[i]))


		nzi = np.array(tmp,dtype=np.uint64)
		HS = np.array(tmpH,dtype=np.uint16)

		save_data(nzi,fname,fl='mm')
		save_data(HS,fnameH,fl='mm')
		#####################################

		tI=[int(GH.I[v]) for v in nzi]
		md_i = len(tI)/2

		f = open_file(path2save + 'hash_stat_smp_' + str(ind) + '.txt', 'w')
		f.write('smp - ' + str(ind) + '; n-z el. - ' + str(len(tI)) + '; % of n-z el. to hash-size - ' + str(SafeRt(len(tI),hmax)) + '; median - ' + str(SafeRt(tI[md_i],hmax)) + '\n')
		f.close()

		H = get_counts(tI,hmax)
		
		fig, ax = plt.subplots()
		ax.stem(range(len(H)), H)
		fig.savefig(path2save + 'hash_hist_smp_' + str(ind) + '.png',format='png')
		plt.close(fig)

		del nzi,tI

