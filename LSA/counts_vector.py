class Counts_Vector():

	def __init__(self):
		self.I = []
		self.V = []

	def make_hist(self,LS):
		self.I = []
		self.V = []
		if len(LS) == 0:
			return

		LS.sort()
		L =len(LS)
		I = [0] * (L)
		V = [0] * (L)
		i = 0
		r = 0

		I[r] = LS[i]
		V[r] += 1
		i += 1
		while i < L:
			if LS[i] != I[r]:
				#print 'r - ',r
				r += 1
				I[r] = LS[i]
			V[r] += 1
			i += 1
		self.I = I[:(r+1)]
		self.V = V[:(r+1)]

	def where(self,index):
		ri = Counts_Vector.get_position(self.I,index)
		return ri

	@staticmethod
	def get_position(I,index):

		ln = len(I)
		if ln == 0:
			return 0
		else:
			li = 0
			ri = ln-1
			if I[li] > index:
				return 0
			if I[ri] < index:
				return ln

			ci = (ri+li)/2
			while (ri - li > 1) and (I[ci] != index) and (I[li] != index) and (I[ri] != index):
				if I[ci] < index:
					li = ci
				if I[ci] > index:
					ri = ci
				ci = (ri+li)/2

			if I[ci] == index:
				return ci
			if I[li] == index:
				return li
			if I[ri] == index:
				return ri
			return ri


	def add(self,H):
		self.I,self.V = Counts_Vector.merge(self.I,self.V,H.I,H.V)

	def add_IV(self,I,V):
		self.I,self.V = Counts_Vector.merge(self.I,self.V,I,V)

	def prod_IV(self,I,V):
		self.I,self.V = Counts_Vector.intersect(self.I,self.V,I,V)

	@staticmethod
	def merge(I1,V1,I2,V2):
		i1 = 0
		i2 = 0
		lI1 = len(I1)
		lI2 = len(I2)
		r = 0
		I = [0] * (lI1 + lI2)
		V = [0] * (lI1 + lI2)
		
		while (i1 < lI1) or (i2 < lI2):
			if (i1 == lI1) and (i2 < lI2):
				ind = I2[i2]
				val = V2[i2]
				i2 += 1
			elif (i1 < lI1) and (i2 == lI2):
				ind = I1[i1]
				val = V1[i1]
				i1 += 1
			else:
				if I1[i1] < I2[i2]:
					ind = I1[i1]
					val = V1[i1]
					i1 += 1
				elif I2[i2] < I1[i1]:
					ind = I2[i2]
					val = V2[i2]
					i2 += 1
				else:
					ind = I1[i1]
					val = V1[i1] + V2[i2]
					i1 += 1
					i2 += 1
			I[r] = ind
			V[r] = val
			r += 1

		return I[:r],V[:r]

	@staticmethod
	def intersect(I1,V1,I2,V2):
		i1 = 0
		i2 = 0
		lI1 = len(I1)
		lI2 = len(I2)
		r = 0
		I = [0] * (max(lI1, lI2))
		V = [0] * (max(lI1, lI2))

		while (i1 < lI1) or (i2 < lI2):
			if I1[i1] == I2[i2]:
				I[r] = I1[i1]
				V[r] = V1[i1] * V2[i2]
				r += 1
				i1 += 1
				i2 += 1
			else:
				if I1[i1] < I2[i2]:
					i1 += 1
				else:
					i2 += 1
			if (i1 == lI1) or (i2 == lI2):
				break

		return I[:r],V[:r]
