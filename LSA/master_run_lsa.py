#!/usr/bin/env python
import sys,getopt,time,os,inspect
from lsa_job import *
from job_vars import JobVars
currentDir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
rootDir = os.path.dirname(currentDir) + '/'

help_message = "usage example: python SLURM/master.py -p project_name"

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			prj = arg
			try:
				JV = JobVars(rootDir + 'PRJ/' + prj)
			except:
				print arg +' is not a known project file.'
				print 'check file in PRj/ dir'
				print help_message
				sys.exit(2)


	JV.add('prjID',prj)
	JV.add('jobID','Master')
	JV.make_prj_space()
	
	sleep_time = int(JV.wts)
        MSR = JV.MSR.split(',')
        for job in MSR:
		print time.ctime(),prj,'-',job

		JV.jobID = job

		sname,oname,ename = JV.make_slurm_job()

		time.sleep(sleep_time) # seconds

		pid = run_slurm_job(sname)

		wait_for_slurm_jobs(pid, sleep_time, job = job)
		e_dir = get_dir_name(ename)
		e_size = file_size(e_dir)
		while  e_size < 0:
			time.sleep(sleep_time) # seconds
			e_size = file_size(e_dir)

		if e_size > 0:
			print 'Error: check ' + ename +  ' files'
			raise NameError(job + ' job was finished with error')

		print time.ctime(),job,'log files were created'

	print time.ctime(), 'Master script is finished, check for results in', JV.oDir
