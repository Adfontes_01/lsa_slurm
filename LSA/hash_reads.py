#!/usr/bin/env python

import glob,os
import sys, getopt
import gzip,time
from fastq_reader import Fastq_Reader
from lsa_job import *
from lsa_math import kmer_bins
from job_vars import JobVars

help_message = 'usage example: python hash_fastq_reads.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg


        JV = JobVars(pDir)
	FP = load_data(JV.oDir + JV.sdrJOB + '/fileList.txt',fl='str')

	AS,step = JV.get_array_size('files')
	HO = Fastq_Reader(JV.oDir)

	print 'start wheel loading'
	HO.load_wheel()

	for i in range(step):
		ind = step*fr + i
		if ind >= len(FP):
			break

		file_name = get_file_name(FP[ind])
		file_name = cut_sufix_from_name(file_name,sfx='.fastq')
		smpDIR = get_prefix_from_name(file_name) + '/'

		hFile = JV.oDir + JV.sdrHASH + smpDIR + file_name + '.hashq.gz'
		print 'fr - ',fr,', i - ',i,', ind - ',ind, ', file_name -  ',file_name
		'''
		flg = os.path.isfile(hFile)
		if flg:
			try:
				print 'found on disk'
				#tLS = load_data(hFile,fl='list')
				#del tLS
				flg = True
				print 'saved correctly'
			except:
				flg = False
				print 'saved with error, need to be recalculated'

		if flg:
			continue
		'''

		t1 = time.time()
		f = open_file(FP[ind])
		h = open_file(hFile,arg='w')
		A = []
		reads_hashed = 0
		while A is not None:
			A,B = HO.generator_to_bins(HO.read_generator(f,max_reads=1024,verbose_ids=False,check_qc=HO.check_qc),rc=HO.reverse_compliment)
			HASH = kmer_bins(A,B)

			for ls in HASH:
				ss = ','.join([str(x) for x in ls]) + '\n'
				h.write(ss)
			reads_hashed += len(HASH)

		t2 = time.time()
		print 'reads hashed - ',reads_hashed,', time to hash reads - ',t2-t1
		h.close()
		f.close()
		del A,B,HASH,f,h
