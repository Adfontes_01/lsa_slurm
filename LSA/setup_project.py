#!/usr/bin/env python

import sys,getopt,os
from fastq_reader import Fastq_Reader
from lsa_job import *
from job_vars import JobVars

help_message = "usage example: python define_files.py -o /project/home/"

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	JV.get_files_info()


	h  = int(JV.hSize)
	c  = int(JV.cSize)
	Lr = int(JV.nbr_NR)
	Km = int(JV.kMer)
	Rf = int(JV.fReads)
	Nf = int(JV.nbr_F)
	Ns = int(JV.nbr_S)
	Cr = 2
	Sc = 10

	DS_h = h * (Lr - Km +1) * Rf * Nf / (4*Cr)
	DS_c =  (Ns * (2**(h+1)) * 5) / Sc
	
	df = os.popen('df -B 1 ' + JV.oDir).read()

	df = int(df.split()[10])
	sizeT = 2**40
	print 'Total free disc space  - ' + str(df/sizeT) + ' T'
	print '# of columns in chunk - ' + str(2**c)
	print '# of hash chunks ~ ' + str(2**(h-c-2))
	print 'Total required  disc space for the task - ' + str((DS_h+DS_c)/sizeT) + ' T'
	print 'Ratio of the required  disc space for the task - ' + str((DS_h+DS_c)/df) + ' %'
	print 'where,'
	print 'Total required  disc space for hashes - ' + str(DS_h/sizeT) + ' T'
	print 'Total required  disc space for counts and indices - ' + str(DS_c/sizeT) + ' T'



	
	#the total max requireed disc space for hashes in bytes:
	#DS_h = (0.25*h) * (Lr - Km +1) * Rf * Nf * Cr

	#For the conditioned files now it can require:
	#DS_c = Sc * (Nf * (2 ^(h+1)) + Nf * (2 ^(h+3))) = Sc * Ns * (2 ^(h+1)) * 5

	#where:
	#h  - hash-size
	#Lr - read length
	#Km - kmer-length
	#Rf - reads per file
	#Nf - number of files
	#Ns - number of samples
	#Cr - compression ratio for gz file
	#Sc - sparseness (% of non-zero elements)

	#e.g., for our last case:
	#h - 30
	#Lr - 101
	#Km - 25
	#Rf - 250000
	#Nf - 79167
	#Ns - 176
	#Cr - 0.5
	#Sc - 0.2

	#for .hash.gz files
	#DS_h  = 5714867812500 (5,2 T)
	#for .hash.condition files
	#DS_c =  226774273228 (211 G)

