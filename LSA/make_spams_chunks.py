#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars
import  scipy.optimize as sop
import spams

help_message = 'usage example: python make_corpus_chunks.py -r 0 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in '-h':
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')

	HO = StreamingEigenhashes(JV.oDir)
	path2spams = HO.oDir + HO.sdrTMP + 'spams/'

	HO.open_AM_files()

	pos = False
	if 'nmf' in HO.decType:
		pos = True 

	U = None
	if os.path.isfile(path2spams + 'T.npy'):
		U = load_data(path2spams + 'T.npy',fl='npy')
		U = np.asfortranarray(U)
		
	prc = False
	for i in range(step):

		ind = step*fr + i
		if ind >= HO.nbr_C:
			break

		ind = random.randint(step*fr,min(HO.nbr_C,step*(fr+1))-1)
		prc = True
		print fr,ind,step,HO.nbr_C
		print 0
		M =  HO.get_full_hash_chunk(ind)
		print 1
		M = np.asfortranarray(M)
		print 2
		U = spams.trainDL(M,D=U,K = HO.decRank,lambda1=0.1,lambda2=0.05,batchsize=M.shape[1],verbose=False,posD=pos,posAlpha=pos,mode=2)
		print 3
		nr = np.sqrt(np.sum(U**2,0))
		U = U / nr
		break

	if prc:
		fname = path2spams + 'c_' + str(fr) + '.npy'
		save_data(U,fname,fl='npy')

	HO.close_AM_files()		
