#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from lsa_job import *
from job_vars import JobVars
import yaml
from collections import defaultdict
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def merge_LS2file(LS,p2file):
	ff = open_file(p2file,'w')
	cn = 0
	for ls in LS:
		tf = open_file(ls)
		ln = 'dummy'
		while ln:
			ln = tf.readline()
			if ln:
				cn += 1
				ff.write(ln)
		tf.close()
		
	ff.close()
	return cn

def get_profile_dicts(fname):

	Dv = defaultdict(float)
	Dn = defaultdict(str)

	txt = load_data(fname,fl='str')
	for li in xrange(len(txt)):
		ln = txt[li].split()
		if len(ln)>1:
			OK = True
			try:
				col = int(ln[0])
				name = ln[1]
				val = float(ln[-1])
			except:
				OK = False
			if OK:
				if name != 'superkingdom':
					Dv[col] = val
					Dn[col] = name
	return Dv,Dn

def push_D_in_A(x,Dv,Dn,name='all'):
	y=np.zeros(x.size)
	for k,v in Dv.iteritems():
		OK = True
		if name != 'all':
			if Dn[k] != name:
				OK = False
		if OK:
			ind = np.where(k==x)
			y[ind] = v
	return y

def meta_profile_pair(SL,NBC):

	Di,Dnm = define_TaxID_names(SL,NBC)

	for smp in SL:
		print '--------------------------------'
		for NM in Dnm:

			cmp0 = 0
			for cmp1 in range(1,len(NBC)):
				print NM,NBC[cmp0],NBC[cmp1]
				fname = JV.oDir + '/samples/metap_ch_'+str(NBC[cmp0])+'/' + smp +'/merged.fastq.profile'
				Dv0,Dn0 = get_profile_dicts(fname)
				fname = JV.oDir + '/samples/metap_ch_'+str(NBC[cmp1])+'/' + smp +'/merged.fastq.profile'
				Dv1,Dn1 = get_profile_dicts(fname)

				fig, ax = plt.subplots()
				plt.hold(True)
				x_ar=np.array(Di)#[:100]
				b_ar=np.array(range(x_ar.size))#[:100]

				y_ar0=push_D_in_A(x_ar,Dv0,Dn0,name=NM)
				y_ar1=push_D_in_A(x_ar,Dv1,Dn1,name=NM)

				nzi = (y_ar0+y_ar1) !=0
				#nzi = (y_ar0*y_ar1) !=0

				markerline, stemlines, baseline = plt.stem(b_ar[nzi],y_ar0[nzi], 'r')
				plt.setp(markerline, color='r',marker='x',markersize=10)
				plt.setp(stemlines, color='r',linewidth=2)

				markerline, stemlines, baseline = plt.stem(b_ar[nzi],y_ar1[nzi], 'g')
				plt.setp(markerline, color='g',marker='+',markersize=10)
				plt.setp(stemlines, color='g',linewidth=1)

				plt.xlabel('TaxID')
				plt.ylabel('PERCENTAGE')
				plt.title(smp+', '+NM)

				plt.hold(False)

				sDir = JV.oDir + '/samples/metap_images/'+'ch_'+str(NBC[cmp0])+'_'+'ch_'+str(NBC[cmp1])+'/' +NM+'/'
				make_dir(sDir)
				fig.savefig(sDir + smp + '.png',format='png')
				plt.close(fig)

def define_TaxID_names(SL,NBC):

	Di = set()
	Dnm = set(['all'])
	for smp in SL:
		for nbc in NBC:
			fname = JV.oDir + '/samples/metap_ch_'+str(nbc)+'/' + smp +'/merged.fastq.profile'
			Dv,Dn = get_profile_dicts(fname)
			Di.update(Dv.keys())
			Dnm.update(Dn.values())

	Di = sorted(list(Di))
	Dnm = sorted(list(Dnm))
	print len(Di),Dnm
	return Di,Dnm


def meta_profile_sample(SL,NBC):

	Di,Dnm = define_TaxID_names(SL,NBC)
	
	for NM in list(Dnm):

		cmp0 = 0
		for cmp1 in range(1,len(NBC)):
			X= np.zeros((len(SL),len(Di)))
			for smpi in range(len(SL)):
				print '--------------------------------'

				print NM,NBC[cmp0],NBC[cmp1]
				fname = JV.oDir + '/samples/metap_ch_'+str(NBC[cmp0])+'/' + SL[smpi] +'/merged.fastq.profile'
				Dv0,Dn0 = get_profile_dicts(fname)
				fname = JV.oDir + '/samples/metap_ch_'+str(NBC[cmp1])+'/' + SL[smpi] +'/merged.fastq.profile'
				Dv1,Dn1 = get_profile_dicts(fname)

				x_ar=np.array(Di)
				b_ar=np.array(range(x_ar.size))

				y_ar0=push_D_in_A(x_ar,Dv0,Dn0,name=NM)
				y_ar1=push_D_in_A(x_ar,Dv1,Dn1,name=NM)

				nzi = (y_ar0+y_ar1) !=0
				print np.sum(nzi)
				print np.sum(np.abs(y_ar0[nzi] - y_ar1[nzi])!=0)
				X[smpi,b_ar[nzi]] = y_ar0[nzi] - y_ar1[nzi]
			print np.sum(np.sum(np.abs(X),0)!=0),X.shape
			nzi = np.sum(np.abs(X),0)!=0
			X = X[:,nzi]
			print X.shape

			fig, ax = plt.subplots()

			c = ax.pcolor(X, edgecolors='k', linewidths=0)

			fig.colorbar(c, ax=ax)

			
			ax.set_xlim(0,X.shape[1])
			ax.set_yticklabels(SL)
			ax.get_xaxis().set_visible(False)

			plt.title(NM)
			fig.tight_layout()

			sDir = JV.oDir + '/samples/metap_images/'+'ch_'+str(NBC[cmp0])+'_'+'ch_'+str(NBC[cmp1])+'/' +NM+'/'
			print sDir
			make_dir(sDir)

			fig.savefig(sDir +  'heatmap_diff.png',format='png')
			plt.close(fig)
	
			np.savetxt(sDir +  'heatmap_diff.csv', X, delimiter=",")
	
def convert_files2dicts(LS,rName):

	nbr_S = len(LS)
	mxc = 0
	for fl in LS:
		fname = fl[:fl.rfind('/')]
		sname = fname[fname.rfind('/')+1:]
		
		D = defaultdict(float)
		txt = load_data(fl,fl='str')
		for i in xrange(len(txt)):
			ln = txt[i].split()
			if len(ln) > 1:
				if ln[1] == rName:
					cl = int(ln[0])
					vl = float(ln[-1])
					D[cl] = vl
		lv = max(D.keys())
		if lv > mxc:
			mxc = lv
		print fl,len(D),mxc
	
		save_data(D,pDir+'dicts/'+rName+'/'+sname+'.dc',fl='list',dump='pk')
	save_data(mxc+1,pDir+'dicts/'+rName+'/nbr_cols.txt',fl='nbr')

help_message = 'usage example: python kmer_cluster_merge.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg
	'''
        JV = JobVars(pDir)
	SL = load_data(JV.oDir + JV.sdrJOB + '/' + 'sampleList.txt',fl='str')[:20]
	NBC = ['all',1,10,25,50]

	meta_profile_pair(SL,NBC)
	#meta_profile_sample(SL,NBC)
	sys.exit()
	'''

	#''' rName
	pDir = '/env/cns/proj/ADAM/LLDeep/PhyloMatrices/'
	#rName = 'genus'
	#rName = 'species'
	#rName = 'strain'
	#rName = 'class'
	#rName = 'family'
	#rName = 'order'
	#rName = 'phylum'

	LS = get_file_list(PATH = pDir, END = '*.profile')
	print len(LS)
	convert_files2dicts(LS,rName)
	#'''
