#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from lsa_job import *
from job_vars import JobVars
import yaml

		
help_message = 'usage example: python kmer_cluster_merge.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	print JV.p2prt
	FASTAS = glob.glob(JV.p2prt + 'fastas/*.fasta')
	print FASTAS
	wts = 10*int(JV.wts)
	SL = load_data(JV.oDir + JV.sdrJOB + '/sampleList.txt',fl='str')
	FL = load_data(JV.oDir + JV.sdrJOB + '/fileList.txt',fl='str')

	DC = {}
	for sm in SL:
		DC[sm] = [fl for fl in FL if sm in fl]
	print 'DC'


	PIDS = []
	reads_type = 'singleType'

	for sample_name in SL:

		for fasta in FASTAS:


			print fasta

			fasta_name = get_file_name(fasta)
			fasta_name = fasta_name[:fasta_name.rfind('.fasta')]

			odir = get_dir_name(fasta) + fasta_name + '/' + sample_name + '/'

			if os.path.isdir(odir):
				print 'continue'
				continue

			JV.jobID = 'IlMap_sampleID_fastaID'
			sname,oname,ename = JV.make_slurm_job()
			print sname

			sname,oname,ename = JV.convert_slurm_job(sname,oname,ename,sample_name,pref='sample')
			sname,oname,ename = JV.convert_slurm_job(sname,oname,ename,fasta_name,pref='fasta')

			file_text = load_data(sname,fl='str')

			ind = 0
			for st in file_text:
				if 'ilmap \\' in st:
					break
				ind += 1

			new_file_text = file_text[:(ind+1)]

			new_file_text.append('-genome ' + fasta +' \\')
			new_file_text.append('-lib ' + sample_name + '@' + reads_type + ' \\')
			for e in DC[sample_name]:
				new_file_text.append('-f ' + sample_name + '@' + e + ' \\')

			print sname
			if JV.paired_reads:
				new_file_text.append('-argsMem "-p" \\')

			new_file_text.append('-mappingType mem \\')
			new_file_text.append('-tools samtools \\')
			new_file_text.append('-index "is" \\')
			new_file_text.append('-rmUnmapped \\')
			new_file_text.append('-rmDup 1 \\')
			new_file_text.append('-bamFilter "-i 99 -a 97"  \\')
			new_file_text.append('-uniqFilter \\')
			new_file_text.append('-rmBam \\')
			new_file_text.append('-nbrTasks ' + JV.taskIlMap +  ' \\')
			new_file_text.append('-output ' + odir)
			new_file_text.append('2>&1')

			new_file_text += file_text[(ind+2):]

			save_data(new_file_text,sname,fl='str')

			print '--------------------'
			pid = run_slurm_job(sname)
			PIDS.append(pid)
			PIDS = check_slurm_pid(PIDS)
			while len(PIDS) > 64:
				print time.ctime(), 'IlMapFasts job is running, wait ' + str(wts) + ' s'
				time.sleep(wts)
				PIDS = check_slurm_pid(PIDS)

	wait_for_slurm_jobs(PIDS,wts,job = 'IlMap')
