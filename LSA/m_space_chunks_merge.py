#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars

help_message = 'usage example: python kmer_corpus.py -r 0 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h','--help'):
			print help_message
			sys.exit()
		elif opt in ('-r','--filerank'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('samples')

	HO = StreamingEigenhashes(JV.oDir)
	HO.load_decomp_model()

	read_dir = JV.oDir + JV.sdrTMP + HO.dec_cls_dir

	for i in range(step):
		ind = step*fr + i
		if ind >= HO.nbr_T:
			break
		print HO.sampleHashFile[ind]

		X = np.memmap(read_dir+'X_'  + str(ind), dtype = np.float32, mode='w+', shape=(HO.nbr_H,))

		st = 0
		for cnk in range(HO.nbr_C):
			file_name = read_dir + 'TR/' + extend_path(str(cnk)) + '/' +  str(ind) + '.npy'
			rch = load_data(file_name,fl='npy')
			X[st:(st+rch.size)] = rch[:]
			st += rch.size
			remove_file(file_name)

		del X

		print st,HO.nbr_H

		X = np.memmap(read_dir+'X_'  + str(ind), dtype = np.float32, mode='r', shape=(HO.nbr_H,))
		#si = np.memmap(read_dir+'SI_'  + str(ind), dtype = np.uint64, mode='w+', shape=(HO.nbr_H,))
		#si[:] = np.argsort(X)
		del X
