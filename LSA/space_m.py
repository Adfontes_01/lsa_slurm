import sys,random,os,inspect
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.art3d as art3d
from numpy import linalg as la
from collections import defaultdict
from lsa_job import load_data,remove_file
import heapq


def dc_from_2d(pt,nnbr):

    from scipy.spatial import ConvexHull
    n2=nnbr/2
    print pt.shape,nnbr,n2

    sz = pt.shape
    fl= np.ones(sz[1],dtype=bool)
    it = 0
    DC = []

    while np.sum(fl)>sz[0]:
        tpt=pt[:,fl]
        ind = np.nonzero(fl)[0]
        tDC = set()
        for i in range(sz[0]-1):
            for j in range(i+1,sz[0]):

                thull = ConvexHull(tpt[[i,j],:].T)
                vind = ind[thull.vertices]
                tDC.update(vind)
                fl[vind] = 0
        DC.append(list(tDC))
        it += 1

    vind = np.nonzero(fl)[0]
    if len(vind):
        fl[vind]=0
        tDC = set()
        tDC.update(vind)
        DC.append(list(tDC))

    return DC

def ind_mat_connects(DC,pt,nnbr):

    sz= pt.shape
    nsq = sz[1]/sz[0]

    IND = np.zeros((nnbr,sz[1]),dtype=np.uint64)

    lDC = len(DC)
    bd = int(2*np.sqrt(lDC)+1)

    for l in range(lDC):

        E0 = np.array(DC[l],dtype=np.uint64)

        Ec = np.copy(E0)
        dl = 1
        while (dl < bd) and  Ec.size < nsq:
            if (l+dl < lDC):
                tE = np.array(DC[l+dl],dtype=np.uint64)
                Ec = np.append(Ec,tE)
            if (l-dl >= 0):
                tE = np.array(DC[l-dl],dtype=np.uint64)
                Ec = np.append(Ec,tE)
            dl += 1

        MX = np.zeros((sz[0],2))
        MX[:,0] = np.amin(pt[:,Ec],1)
        MX[:,1] = np.amax(pt[:,Ec],1)
        dp = (MX[:,1] - MX[:,0])/4

        for i in range(E0.size):
            
            next = True
            tdp = np.copy(dp)
            while next:
                fl = np.ones(Ec.size,dtype=bool)
                for j in range(sz[0]):
                    fl *= ((pt[j,Ec] > (pt[j,E0[i]] - tdp[j])) * (pt[j,Ec] < (pt[j,E0[i]] + tdp[j])))
                if np.sum(fl)>nnbr:
                    next = False
                else:
                    tdp *= 1.61
                    

            tEc = Ec[fl]

            df =  pt[:,tEc].T - pt[:,E0[i]].T
            df = np.sum(df**2,1)
            si = np.argsort(df)
            sEc = tEc[si]

            j = 0
            while (j < nnbr):
                IND[j,E0[i]] = sEc[j+1]
                j+=1

    return IND


def ind_mat_2div_sort(pt,si,ssi,nnbr):

    sz= pt.shape
    nsq = sz[1]/sz[0]

    IND = np.zeros((nnbr,sz[1]),dtype=np.uint64)
    dp = nnbr

    for i in range(sz[1]):

        PS = ssi[:,i]
        next = True
        inc2 = 0
        while next:
            fl = np.zeros(sz[1],dtype=np.uint16)
            for j in range(sz[0]):
                lb = max(0,int(PS[j]-dp))
                ub = min(sz[1]-1,int(PS[j]+dp))
                sbi = si[j,lb:ub]
                fl[sbi] += 1

            sm = np.sum(fl[sbi] == sz[0])
            if sm > nnbr:
                next = False
            else:
                inc2 += 1
                dp *= 1.61

        if inc2:
            inc2 = 0
        else:
            dp /= 1.61
            
        fl = fl[sbi] == sz[0]
        tEc = sbi[fl]

        df =  pt[:,tEc].T - pt[:,i].T
        df = np.sum(df**2,1)
        sid = np.argsort(df)
        sEc = tEc[sid[1:]]

        IND[:,i] = sEc[:nnbr]

    return IND


def ind_mat_2div(pt,nnbr):

    sz= pt.shape
    nsq = sz[1]/sz[0]

    IND = np.zeros((nnbr,sz[1]),dtype=np.uint64)

    MX = np.zeros((sz[0],2))
    MX[:,0] = np.amin(pt,1)
    MX[:,1] = np.amax(pt,1)
    dp = (MX[:,1] - MX[:,0])/pt.size

    for i in range(sz[1]):

        next = True
        inc2 = 0
        while next:
            lb = pt[:,i] - dp
            ub = pt[:,i] + dp
            fl = np.ones(sz[1],dtype=bool)
            for j in range(sz[0]):
                fl *= ((pt[j,:] > lb[j]) * (pt[j,:] < ub[j]))

            if np.sum(fl)>nnbr:
                next = False
            else:
                inc2 += 1
                dp *= 1.61

        if inc2:
            inc2 = 0
        else:
            dp /= 1.61
            
        tEc = np.nonzero(fl)[0]

        df =  pt[:,tEc].T - pt[:,i].T
        df = np.sum(df**2,1)
        si = np.argsort(df)
        sEc = tEc[si[1:]]

        IND[:,i] = sEc[:nnbr]

    return IND

def ind_mat_2div_arr(pt,nnbr):

    sz= pt.shape
    nsq = sz[1]/sz[0]

    IND = np.zeros((nnbr,sz[1]),dtype=np.uint64)

    MX = np.zeros((sz[0],2))
    MX[:,0] = np.amin(pt,1)
    MX[:,1] = np.amax(pt,1)
    dp = (MX[:,1] - MX[:,0])/pt.size

    tt = np.array(range(sz[1]),dtype=np.uint64)
    inc2 = 0
    for i in range(sz[1]):

        next = True
        while next:
            tEc = np.copy(tt)
            lb = pt[:,i] - dp
            ub = pt[:,i] + dp
            for j in range(sz[0]):
                tEc = tEc[pt[j,tEc] < ub[j]]
                tEc = tEc[pt[j,tEc] > lb[j]]
                if tEc.size <= nnbr:
                    dp *= 1.61
                    inc2 = -1
                    break

            if inc2 == -1:
                inc2 = 0
                continue

            inc2 += 1
            next = False

        if inc2 == sz[0]:
            inc2 = 0
            dp /= 1.61
            
        df =  pt[:,tEc].T - pt[:,i].T
        df = np.sum(df**2,1)
        si = np.argsort(df)
        sEc = tEc[si[1:]]

        IND[:,i] = sEc[:nnbr]
    return IND

def find_nnbr_connects(DC,pt,nnbr):

    lDC = len(DC)
    for l in range(lDC):
        E0 = np.array(DC[l].keys(),dtype=np.uint64)

        Enx = np.zeros(0,dtype=np.uint64)
        tl = l+1
        while (Enx.size<=2*E0.size) and (tl <  lDC):
            tE = np.array(DC[tl].keys(),dtype=np.uint64)
            Enx = np.append(Enx,tE)
            tl += 1

        Epr = np.zeros(0,dtype=np.uint64)
        tl = l-1
        while (Epr.size<=2*E0.size) and (tl >=0):
            tE = np.array(DC[tl].keys(),dtype=np.uint64)
            Epr = np.append(Epr,tE)
            tl -= 1
        Ec = np.append(Epr,Enx)

        for i in range(E0.size):
            if len(DC[l][E0[i]]) < nnbr:
                df =  pt[:,Ec].T - pt[:,E0[i]].T
                df = np.sum(df**2,1)
                si = np.argsort(df)
                sEc = Ec[si]

                j = 0
                while (len(DC[l][E0[i]]) < nnbr):
                    DC[l][E0[i]].add(sEc[j])
                    j += 1


    return DC


def plot2color(X,VI,tlt=''):
    uv = np.unique(VI)
    ne = uv.size
    ln = len(X)

    if ln == 1:
        plt.figure()
        for i in range(ne):
            fl = VI == uv[i]
            plt.plot(X[0][fl],np.ones(np.sum(fl)),'.',color=np.random.rand(3))
            plt.text(np.mean(X[0][fl]),1,str(uv[i]))

    elif ln == 2:
        plt.figure()
        for i in range(ne):
            fl = VI == uv[i]
            plt.plot(X[0][fl],X[1][fl],'.',color=np.random.rand(3))
            plt.text(np.mean(X[0][fl]),np.mean(X[1][fl]),str(uv[i]))
    else:
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        for i in range(ne):
            fl = VI == uv[i]
            ax.plot(X[0][fl],X[1][fl],X[2][fl],'.',c=np.random.rand(3))
    plt.title(tlt)
    plt.draw()

def stem3(X,SD):
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1,projection='3d')
    for xi, yi, zi in zip(X[0,:], X[1,:], SD):        
        line=art3d.Line3D(*zip((xi, yi, 0), (xi, yi, zi)), marker='o', markevery=(1, 1))
        ax.add_line(line)
    ax.set_xlim3d(min(X[0,:]), max(X[0,:]))
    ax.set_ylim3d(min(X[1,:]), max(X[1,:]))
    ax.set_zlim3d(min(0,min(SD)), max(SD))    
    plt.draw()

def find_ind_sr(ARR,val):

    ln = len(ARR)
    if ln == 0:
        return 0

    li = 0
    ri = ln-1
    if ARR[li] > val:
        return 0
    if ARR[ri] < val:
        return ln

    ci = (ri+li)/2
    while (ri - li > 1) and (ARR[ci] != val) and (ARR[li] != val) and (ARR[ri] != val):
        if ARR[ci] < val:
            li = ci
        if ARR[ci] > val:
            ri = ci
        ci = (ri+li)/2

    if ARR[ci] == val:
        return ci
    if ARR[li] == val:
        return li
    if ARR[ri] == val:
        return ri
    return ri


def pos_in_sr_arr2d(SI,X,j):
    sz = SI.shape[0]
    PS = np.zeros(sz,dtype=np.uint64)
    for i in range(sz):
        PS[i] = find_ind_sr(X[i,SI[i,:]],X[i,j])
    return PS


def pos_in_sr_arr(SI,X,j):
    sz = len(SI)
    PS = np.zeros(sz,dtype=np.uint64)
    for i in range(sz):
        PS[i] = find_ind_sr(X[i][SI[i]],X[i][j])
    return PS


def extend_indx(X,SI,PS,BND,i):
    sz = [len(SI), SI[0].size]
    bnd = BND[i,:]
    if bnd[0] == 0:
        bnd[1] += 1
        vi = SI[i][bnd[1]]
        vd = X[i][SI[i][bnd[1]]] - X[i][SI[i][PS[i]]]
    elif bnd[1] == sz[1]-1:
        bnd[0] -= 1
        vi  = SI[i][bnd[0]]
        vd = X[i][SI[i][PS[i]]] - X[i][SI[i][bnd[0]]]
    else:
        dl = X[i][SI[i][PS[i]]] - X[i][SI[i][int(bnd[0] - 1)]]
        dr = X[i][SI[i][int(bnd[1] + 1)]] - X[i][SI[i][PS[i]]]
        if dl < dr:
            bnd[0] -= 1
            vi  = SI[i][bnd[0]]
            vd = dl
        else:
            bnd[1] += 1
            vi  = SI[i][bnd[1]]
            vd = dr
    return bnd,vi,vd

def extend_indx_fast_dist(X,SI,PS):

    sz = [len(SI), SI[0].size]
    BND = np.zeros((sz[0],2),dtype=np.uint64)
    p2 = np.zeros(sz[0],dtype=np.float32)

    for i in range(sz[0]):
        BND[i,:] = [PS[i],PS[i]]
        p2[i] = 2*X[i][SI[i][PS[i]]]
        yield SI[i][PS[i]]

    while True:
        for i in range(sz[0]):

            if BND[i,0] == 0:
                BND[i,1] += 1
                yield SI[i][int(BND[i,1])]
            elif BND[i,1] == sz[1]-1:
                BND[i,0] -= 1
                yield SI[i][int(BND[i,0])]
            else:
                if X[i][SI[i][int(BND[i,1] + 1)]] >  (p2[i] - X[i][SI[i][int(BND[i,0] - 1)]]):
                    BND[i,0] -= 1
                    yield SI[i][int(BND[i,0])]
                else:
                    BND[i,1] += 1
                    yield SI[i][int(BND[i,1])]

def extend_indx_fast(SI,PS):

    sz = [len(SI), SI[0].size]
    BND = np.zeros((sz[0],2),dtype=np.uint64)

    for i in range(sz[0]):
        BND[i,:]=[PS[i],PS[i]]
        yield SI[i][PS[i]]
    i = 0
    while True:
        if BND[i,0] > 0:
            BND[i,0] -= 1
            yield SI[i][int(BND[i,0])]
        if BND[i,1] < sz[1]-1:
            BND[i,1] += 1
            yield SI[i][int(BND[i,1])]
        i += 1
        i %= sz[0]

def indx_2div(X,mm,strC,szC):

    sz= [len(X), X[0].size]

    dp = np.zeros(sz[0])
    for i in range(sz[0]):
        dp[i] = (np.max(X[i]) - np.min(X[i]))/(np.sqrt(sz[1]))

    for i in range(szC):
        ind = strC + i
        next = True
        inc2 = 0
        while next:
            fl = np.ones(sz[1],dtype=bool)
            for j in range(sz[0]):
                fl *= ((X[j] > (X[j][ind] - dp[j])) * (X[j] < (X[j][ind] + dp[j])))
            if np.sum(fl)>mm:
                next = False
            else:
                inc2 += 1
                dp *= 1.61

        if inc2:
            inc2 = 0
        else:
            dp /= 1.61


        tEc = np.nonzero(fl)[0]
        df = np.zeros(tEc.size)
        for j in range(sz[0]):
            df += (X[j][tEc] - X[j][ind])**2
        si = np.argsort(df)
        sEc = tEc[si[1:]]

        yield sEc[:mm]


def circle_indx(X,SI,mm,jj):
    sz = [len(X), X[0].size]
    PS = pos_in_sr_arr(SI,X,jj)

    C = np.zeros(sz[1],dtype=np.uint16)
    IND = []
    VAL = []
    BND = np.zeros((sz[0],2),dtype=np.uint64)
    fl = np.ones(sz[0],dtype=bool)

    k = 0
    for i in range(sz[0]):
        BND[i,:]=[PS[i],PS[i]]
        ind  = SI[i][PS[i]]
        C[ind] += 1

    vD = np.zeros(sz[0])
    vI = np.zeros(sz[0],dtype=np.uint64)
    for i in range(sz[0]):
        bnd,vi,vd =  extend_indx(X,SI,PS,BND,i)
        BND[i,:] = bnd
        vI[i] = vi
        vD[i] = vd

    while k < mm:
        mi = np.nonzero(np.min(vD[fl]) == vD)[0][0]
        ind = vI[mi]
        C[ind] += 1
        if C[ind] == sz[0]:
            ds = np.sum([(X[i][jj]-X[i][ind])**2 for i in range(sz[0]) ])
            ii = find_ind_sr(VAL,ds)
            IND.insert(ii,ind)
            VAL.insert(ii,ds)
            k += 1
            if ii < mm:
                k = 0

        bnd,vi,vd =  extend_indx(X,SI,PS,BND,mi)
        BND[mi,:] = bnd
        vI[mi] = vi
        vD[mi] = vd
        if (bnd[0]==0) and (bnd[1]==(sz[1]-1)):
            fl[mi] = False
    return np.array(IND[:mm],dtype=np.uint64),np.array(VAL[:mm])


def circle_indx_fast(X,SI,mm,jj):
    sz = [len(X), X[0].size]
    PS = pos_in_sr_arr(SI,X,jj)
    genIND =  extend_indx_fast_dist(X,SI,PS)

    C = np.zeros(sz[1],dtype=np.uint16)
    IND = []
    VAL = []

    k = 0

    while k <= mm:
        vi =  next(genIND)
        C[vi] += 1
        if C[vi] == sz[0]:
            ds = np.sum([(X[l][jj]-X[l][vi])**2 for l in range(sz[0]) ])
            ln = len(VAL)
            if ln <= mm:
                ii = find_ind_sr(VAL,ds)
                IND = IND[:ii] + [vi] + IND[ii:]
                VAL = VAL[:ii] + [ds] + VAL[ii:]
            elif ds < VAL[-1]:
                ii = find_ind_sr(VAL,ds)
                IND = IND[:ii] + [vi] + IND[ii:mm]
                VAL = VAL[:ii] + [ds] + VAL[ii:mm]
            k += 1

    return np.array(IND[1:],dtype=np.uint64),np.array(VAL[1:])

def ind_mat(X,SI,SSI,nbr):

    ln = len(X[0])
    IND = np.zeros((nbr,ln),dtype=np.uint64)
    for j in range(ln):
        tI = circle_indx_inc_2s(X,SI,SSI,nbr,j)
        IND[:,j] = tI[:nbr]
    return IND

def iter_indx_direct(X,IND,bnd,jj):
    dm = len(X)

    P = np.ones(X[0].size,dtype=bool)
    L = []
    C = [jj]
    pn = -1
    it = 0
    while (len(L) <= (bnd+dm)) and (len(L) != pn):
        N = {}
        it += 1
        pn = len(L)
        for vi in C:
            if P[vi]:
                L.append(vi)
                P[vi] = False
                
                for vj in IND[:,vi]:
                    N[vj]=1
        C = N.keys()

    nzi = np.array(L,dtype=np.uint64)
    d = np.zeros(nzi.size,dtype=X[0].dtype)
    for i in range(dm):
        d+= ((X[i][nzi]-X[i][jj])**2)

    si = np.argsort(d)
    nzi = nzi[si[1:bnd+1]]
    d = d[si[1:bnd+1]]
    return nzi,d


def circle_indx_direct(X,SI,mm,jj):

    sz = [len(X), X[0].size]
    PS = pos_in_sr_arr(SI,X,jj)
    genIND =  extend_indx_fast(SI,PS)

    C= np.zeros(sz[1],dtype=np.uint16)
    nb = 0
    j = 0
    nzi = np.zeros(mm,dtype=np.uint64)
    while nb < mm:
        vi =  next(genIND)
        C[vi] += 1
        if C[vi] == sz[0]:
            if vi != jj:
                nzi[nb] = vi
                nb += 1
        j += 1
    return nzi

def circle_indx_inc(X,SI,SSI,mm,jj):
    print 'jj',jj
    sz = [len(SI), X[0].size]

    PS = np.zeros(sz[0])
    for i in range(sz[0]):
        PS[i] = SSI[i][jj]
    
    K = np.array([jj],dtype=np.uint64)
    sm = 0
    psm = -1
    while sm != psm:
        D = {}
        for el in K:

            for i in range(sz[0]):
                    if SSI[i][el] < mm:
                        sr = 0
                        sp = 2*mm
                    elif SSI[i][el] > (sz[1]-mm):
                        sr = sz[1]-2*mm
                        sp = sz[1]
                    else:
                        sr = int(SSI[i][el])-2*mm
                        sp = int(SSI[i][el])+2*mm
                    for v in SI[i][int(sr):int(sp)]:
                        D[v]=1

        print 'next',len(D)
        K = np.array(D.keys(),dtype=np.uint64)
        ds = np.zeros(len(K))
        t = np.zeros(len(K))
        for i in range(sz[0]):
            t += np.abs(X[i][K]-float(X[i][jj]))
            ds += np.abs(SSI[i][K]-PS[i])
        ds = np.log(1.0+ds)*np.log(1.0+t)

        si = np.argsort(ds)
        K = K[si[:(mm+1)]]
        psm = sm
        sm = np.sum(ds[si[:(mm+1)]])

    return K[1:(mm+1)]


def circle_indx_inc_2s(X,SI,SSI,mm,jj):

    sz = [len(SI), X[0].size]
    print 'jj',jj,sz

    mm2 = 2*mm
    K = np.array([jj],dtype=np.uint64)

    psm = sz[1]
    sm = 0
    while psm !=  sm:
        D = set()
        for el in K:
            for i in range(sz[0]):
                if X[i][el]:
                    #'''
                    sr = int(SSI[i][el])-1
                    sp = int(SSI[i][el])+1
                    if sr < 0:
                        sr = 0
                        sp = 2
                    elif sp > sz[1]:
                        sr = sz[1]-2
                        sp = sz[1]
                    #'''
                    D.update(SI[i][int(sr):int(sp)])
    
        K = np.array(list(D),dtype=np.uint64)
        ds = np.zeros(K.size)
        for i in range(sz[0]):
            ds += np.abs(X[i][K]-float(X[i][jj]))
        si = np.argsort(ds)
        K = K[si]
        ds = ds[si]

        K = K[:mm2]
        psm = sm
        sm = np.sum(ds[:mm2])
    ar=np.array(range(K.size),dtype=np.uint64) 
    D = np.zeros(K.size,dtype=np.uint64)
    for i in range(sz[0]):
        fl = X[i][jj]>=X[i][K]
        D[fl] += ar[:fl.sum()]

        fl = ~fl
        D[fl] += ar[:fl.sum()]

    si = np.argsort(D)

    return K[si[1:(mm+1)]]

def circle_indx_sort(X,SI,mm,jj):

    sz = [len(X), X[0].size]
    V = np.zeros(sz[1])

    PS = pos_in_sr_arr(SI,X,jj)
    for i in range(sz[0]):
        V[SI[i][:(int(PS[i]+1))]] += range(int(PS[i]),-1,-1)
        V[SI[i][int(PS[i]):]] += range(int(sz[1]-PS[i]))
    V[jj] = sz[0]*sz[1]
    thr = heapq.nsmallest(mm, V)[-1]
    nzi=np.nonzero(V<=thr)[0]

    return nzi

def circle_indx_intersect(X,SI,mm,jj):

    sz = [len(X), X[0].size]
    m2 = int((sz[1]/sz[0])*(float(mm)/sz[1])**(1.0/sz[0]))
    PS = pos_in_sr_arr(SI,X,jj)
    next = True
    while next:
        next = False
        S = np.zeros(sz[1],dtype=np.uint16)
        V = np.zeros(sz[1])
        for i in range(sz[0]):
            if PS[i] < m2:
                sr = 0
                sp = 2*m2
            elif PS[i] > (sz[1]-m2):
                sr = sz[1]-2*m2
                sp = sz[1]
            else:
                sr = int(PS[i])-m2
                sp = int(PS[i])+m2

            S[SI[i][int(sr):int(sp)]] += 1
            V[SI[i][sr:int(PS[i])]] += range(int(PS[i]-sr-1),-1,-1)
            V[SI[i][int(PS[i]):sp]] += range(int(sp-PS[i]))
        S[jj] = 0
        nzi=np.nonzero(S==sz[0])[0]
        if nzi.size < mm:
            m2*=2
            next = True
    asi = np.argsort(V[nzi])
    return nzi[asi[:mm]]

def dist_p(x,xj):
    d = xj.size
    df = 0.0
    for i in range(d):
        df += (x[i,:]-xj[i])**2
    return df

def get_m_indx(x,xj,Ij,m):
    
    df = dist_p(x[:,Ij],xj)
    si = np.argsort(df)
    si = si[:m]
    return df[si],Ij[si]

def uv_un_D(VI):

    C = defaultdict(int)
    for v in VI:
        C[v] += 1
    cl = len(C)

    uv=np.zeros(cl,dtype=np.uint64)
    un=np.zeros(cl,dtype=np.uint64)
    D = {}
    i = 0
    for k in sorted(C.keys()):
        uv[i] = k
        un[i] = C[k]
        D[k] = i
        i += 1
    return uv,un,D

def mat_filter_connects(X,IND,VI,LS,mb):

    for j in LS:
        tI,tV =  iter_indx_direct(X,IND,mb,j)
        if np.any(VI[j] != VI[tI]):
                tD = defaultdict(int)
                for v in VI[tI]:
                    tD[v]+=1
                ky = tD.keys()
                va = tD.values()
                nv = ky[np.argmax(va)]
                VI[j] = nv

    return VI
    
def mat_count_connects(X,IND,VI,LS,m):

    uv,un,D = uv_un_D(VI)
    cl = uv.size
    print 'un.size',cl

    from scipy.sparse import dok_matrix
    CN = dok_matrix((cl, cl), dtype=np.uint64)
    for j in LS:
        tI,tV =  iter_indx_direct(X,IND,m,j)

        if np.any(VI[j] != VI[tI]):
            T={}
            for k in tI:
                T[VI[k]] = 1
            for k in T.keys():
                CN[D[VI[j]],D[k]] += 1

    return CN

def threshold_connects(D,thr):

    for (i,j),v in D.items():
        if v <=thr:
            D[i,j]=0
    return D


def merge_sparse(FL,cl):

    from scipy.sparse import dok_matrix
    CN = dok_matrix((cl, cl), dtype=np.uint64)

    print 'files # -',len(FL)
    for i in range(len(FL)):
            tCN = load_data(FL[i],fl='list',dump='pk')
            
            for k,v in tCN.items():
                CN[k[0],k[1]] += v
            print i,len(tCN.items()),len(CN.items())
    return CN

def dict_row_col(CN,ind):
    v= CN[ind,:].toarray()+CN[:,ind].toarray().reshape(-1)
    return v[0]

def mat_merge_small(VI,CN,gm):

    uv,un,D = uv_un_D(VI)
    mni=np.argmin(un)
    while un[mni]<=gm:
        CN[mni,mni] = 0
        v= dict_row_col(CN,mni)
        mxi=np.argmax(v)
        if v[mxi]:
            VI[uv[mni]==VI] = uv[mxi]
            un[mxi] += un[mni]
            uv[mni] = uv[mxi]
        un[mni] = 2*gm
        mni=np.argmin(un)

    print 'un',un.size,un
    print 'uv',uv.size,uv
    return VI

def gauss_mask_dists(X,VI,CN,mS_th_ds=5):

    uv,un,D = uv_un_D(VI)

    d = len(X)
    mX = np.zeros((uv.size,d))
    for i in range(uv.size):
        fl = VI == uv[i]
        for j in range(d):
            mX[i,j] = np.median(X[j][fl])

    COV = np.zeros((d,d,un.size))
    iCOV = np.zeros((d,d,un.size))
    from sklearn.covariance import MinCovDet

    for i in range(uv.size):
        fl = VI == uv[i]
        tX = np.zeros((np.sum(fl),d))
        for j in range(d):
            tX[:,j] = X[j][fl] - mX[i,j]

        COV[:,:,i] = np.dot(tX.T,tX)/(np.sum(fl))
        iCOV[:,:,i] = la.pinv(COV[:,:,i])


    for i in range(uv.size):
        dX = np.copy(mX)
        for j in range(d):
            dX[:,j] -= mX[i,j]

        for j in range(un.size):
            if CN[i,j]:
                df = 0.5*np.dot(dX[j,:],np.dot(iCOV[:,:,i],dX[j,:]))
                if df > mS_th_ds:
                    CN[i,j]=0
        
    return CN

def gauss_merge_connected(X,VI,CN):

    uv,un,D = uv_un_D(VI)
    n = VI.size

    cl = 0
    while np.sum(un):
        print cl,'------------------'
        mi = np.argmax(un)
        tm = np.array([mi])
        j = 0
        while j < tm.size:
            msk = dict_row_col(CN,tm[j]) * (un != 0)
            ni = np.nonzero(msk)[0]
            for nb in ni:
                if not np.sum(tm==nb):
                    tm = np.append(tm,nb)
            j += 1

        un[tm]=0
        for nb in tm:
            VI[VI==uv[nb]]=uv[tm[0]]
        cl +=1

    if type(VI) is np.ndarray:
            return VI
