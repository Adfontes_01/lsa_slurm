#!/usr/bin/env python

import glob,os
from lsa_job import *

def sm_size(FL):
    if len(FL) != 0:

        s=0
        for fl in FL:
            s += file_size(fl)
    return s

def sm_lines(FL):
    sl = 0
    if len(FL) != 0:
        print FL[0][:FL[0].rfind('/')]
        for fl in FL:
            sl += int(os.popen('wc -l '+ fl).read().split()[0])
    return sl

def get_cluster_stats(path):
    MAS = load_nCLS(path)

    print MAS
    S=[]
    SL=[]
    for i in range(MAS):
        FL=glob.glob(path+str(i+1) + '/*.fastq')
        S.append(sm_size(FL))
        SL.append(sm_lines(FL))
    ST=[str(i)+'\t'+str(S[i]) + '\t' +str(SL[i])+'\n' for i in range(MAS)]
    f=open_file(path+'cluster_stats.txt','w')
    f.writelines(ST)
    f.close()

def show_dir_size(path):
    FL=glob.glob(path + '*.fastq')
    s=sm_size(FL)
    sl=sm_lines(FL)
    print 's - ',s,' sl - ',sl

def get_samples_size(path):
    LS1 = glob.glob(path + '/SRS*')
    LS1.sort()
    if len(LS1)==0:
        print 'the number of samples is  0 !!!!!!!'
        return
    sl1=[]
    for i in range(len(LS1)):
        FL1=glob.glob(LS1[i] + '/*.fastq*')
        s1 = sm_size(FL1)
        sl1.append(s1)
        if s1==0:
            print 'empty sample',
            return
    fname='samples_stats.txt'
    ST=[str(i)+'\t'+str(sl1[i]) + '\n' for i in range(len(sl1))]
    ST.append('total size in bytes: ' + str(sum(sl1)) + '\n')
    f=open_file(path+fname,'w')
    f.writelines(ST)
    f.close()

def compare_init_merged(path1,path2):
    LS1 = glob.glob(path1 + '/SRS*')
    LS2 = glob.glob(path2 + '/SRS*')
    LS1.sort()
    LS2.sort()
    if len(LS1)!=len(LS2):
        print 'the number of samples is different!!!!!!!'
        return
    sl1=[]
    sl2=[]
    for i in range(len(LS1)):
        print '--------------------------------------------'
        print 'i - '+str(i)+' , start sample ' + LS1[i][LS1[i].rfind('/')+1:]
        FL1=glob.glob(LS1[i] + '/*.fastq*')
        FL2=glob.glob(LS2[i] + '/*.fastq*')
        s1 = sm_size(FL1)
        s2 = sm_size(FL2)
        sl1.append(s1)
        sl2.append(s2)
        print 's1 - ',s1,' s2 - ',s2
        if s1!=s2:
            print 'Check the data !!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            return
    print '-------------------------------------'
    print 'done ! all data are in the same size'
    ST=[str(i)+'\t'+str(sl1[i]) + '\t' +str(sl2[i])+'\n' for i in range(len(sl1))]
    f=open_file(path1+'cluster_stats.txt','w')
    f.writelines(ST)
    f.close()

def check_paired_files(path):
    print 'check if paired files exist in'
    print path
    LS = glob.glob(path + 'SRS*')
    if len(LS)==0:
        print 'no samples, exit'
        return
    LS.sort()
    SL=[]
    for i in range(len(LS)):
        FL=glob.glob(LS[i] + '/*.fastq*')
        if len(FL)%2 != 0:
            print '--------------------------------------------'
            print 'i - '+str(i)+' , start sample ' + LS[i][LS[i].rfind('/')+1:]
            print 'no paired fasq file'
            return
    print 'done OK--------------------------------------------'


if __name__ == "__main__":

    #path = '/env/cns/proj/projet_BWG/scratch/merged/'
    path = '/genoscope/merged/'
    get_samples_size(path)

    '''
    path = '/env/cns/bigtmp1/okyrgyzo/merged_pair1/clusters/thr_0.9/'
    get_clusters_stats(path)
    '''
    
    '''
    #path1 = '/env/cns/bigtmp1/okyrgyzo/cmp/1/SRS475568'
    #path2 = '/env/cns/bigtmp1/okyrgyzo/cmp/2/SRS475568'
    path1 = '/env/cns/bigtmp1/okyrgyzo/merged_pair1/partitions/thr_0.9/9/'
    path2 = '/env/cns/bigtmp1/okyrgyzo/merged_pair1/clusters/thr_0.9/9/'
    show_dir_size(path1)
    show_dir_size(path2)
    '''

    '''
    path1 = '/env/cns/proj/projet_BWG/scratch/'
    path2 = '/env/cns/proj/projet_BWG/scratch/merged/'
    compare_init_merged(path1,path2)
    '''

    '''
    path1 = '/genoscope/merged/'
    path2 = '/genoscope/'
    compare_init_merged(path1,path2)
    '''

    '''
    #path = '/env/cns/proj/projet_BWG/scratch/'
    path = '/genoscope/'
    check_paired_files(path)
    '''
