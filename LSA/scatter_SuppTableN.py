#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from lsa_names import get_file_name,get_dir_name
from mpl_toolkits.mplot3d import Axes3D 

from lsa_job import *
from lsa_stats import filter_fasta_nodes,get_node_fasta
from job_vars import JobVars
from collections import defaultdict
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
#import matplotlib.cm as cm

help_message = 'usage example: python script_name.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'h')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()


	p2Tab = '/env/cns/proj/ADAM/LLDeep/OK_Article/Figs/SuppTableN.txt'
	gTB = load_data(p2Tab,fl='str')

	print len(gTB)
	print gTB
	TX = []
	for el in gTB:
		if ('/' not in el) and el:
			print el
			tel = el[6:].strip().split('&')
			TX.append(tel)
			print len(tel)
	print len(TX)
	MX=np.zeros((len(TX),len(TX[0])),dtype=int)
	for i in range(len(TX)):
		el = TX[i]
		for j in range(len(TX[i])):
			MX[i,j] = int(TX[i][j])
	print MX
	print(MX.shape)
	MX = np.roll(MX,7,axis=0)
	#print MX
	#sys.exit()
	cm = plt.cm.get_cmap('jet')
	#plt.figure()
	fig, ax = plt.subplots()

	rcl = np.random.rand(MX.shape[0],3)
	for i in range(MX.shape[0]):
		print(len(MX[i,0]*np.ones(MX.shape[1]-1)))
		print(len(np.array(range(1,MX.shape[1]+1))))
		print(len(MX[i,1:]))
		#ax.scatter(MX[i,0]*np.ones(MX.shape[1]-1),np.array(range(1,MX.shape[1])),s=2*MX[i,1:],c=rcl[i,:],linewidth=1,alpha=0.5)
		ax.scatter(i*np.ones(MX.shape[1]-1),np.array(range(1,MX.shape[1])),s=3*MX[i,1:],c=rcl[i,:],linewidth=1,alpha=0.75)

	ax.grid(True)
	ax.set_xlim([-1,MX.shape[0]])
	plt.xticks(np.arange(MX.shape[0]), MX[:,0], rotation='vertical')
	ax.set_ylim([0,MX.shape[1]])
	ax.set_title('SuppTableN')
	ax.set_xlabel('genome #')
	ax.set_ylabel('sample #')
	fig.tight_layout()
	fig.savefig(p2Tab + '.pdf',format='pdf',bbox_inches='tight')
	plt.close(fig)

	#fig, ax = plt.subplots()
	#####################
	'''
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	#x, y = np.random.rand(2, 100) * 4
	#hist, xedges, yedges = np.histogram2d(x, y, bins=4, range=[[0, 4], [0, 4]])

	# Construct arrays for the anchor positions of the 16 bars.
	xpos, ypos = np.meshgrid(xedges[:-1] + 0.25, yedges[:-1] + 0.25, indexing="ij")
	xpos = xpos.ravel()
	ypos = ypos.ravel()
	zpos = 0

	# Construct arrays with the dimensions for the 16 bars.
	dx = dy = 0.5 * np.ones_like(zpos)
	dz = hist.ravel()

	ax.bar3d(xpos, ypos, zpos, dx, dy, dz, zsort='average')
	fig.savefig(p2Tab + '.bar3d.pdf',format='pdf')
	plt.close(fig)
	'''
	#######################
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	#x, y = np.random.rand(2, 100) * 4
	hist = np.copy(MX[:,1:])
	xedges = np.arange(hist.shape[0]+1)
	yedges = np.arange(hist.shape[1]+1)

	# Construct arrays for the anchor positions of the 16 bars.
	# Note: np.meshgrid gives arrays in (ny, nx) so we use 'F' to flatten xpos,
	# ypos in column-major order. For numpy >= 1.7, we could instead call meshgrid
	# with indexing='ij'.
	xpos, ypos = np.meshgrid(xedges[:-1] + 0.25, yedges[:-1] + 0.25)
	xpos = xpos.flatten('F')
	ypos = ypos.flatten('F')
	zpos = np.zeros_like(xpos)

	# Construct arrays with the dimensions for the 16 bars.
	dx = 0.5 * np.ones_like(zpos)
	dy = dx.copy()
	dz = hist.flatten()

	#cmap = cm.get_cmap('jet') # Get desired colormap
	cmap = plt.cm.get_cmap('jet')
	max_height = np.max(dz)   # get range of colorbars
	min_height = np.min(dz)

	# scale each z to [0,1], and get their rgb values
	rgba = [cmap(255*(k-min_height)/max_height) for k in dz] 

	ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color=rgba, zsort='average')
	#ax.bar3d(xpos, ypos, zpos, dx, dy, dz, zsort='average')
	ax.set_xlim([0,21])
	plt.xticks(np.arange(MX.shape[0]), MX[:,0], rotation='vertical')
	ax.set_xlabel('genomes')
	ax.set_zlabel('conting hits')
	ax.set_ylabel('samples')
	fig.tight_layout()
	fig.savefig(p2Tab + '.bar3d.pdf',format='pdf',bbox_inches='tight')
	plt.close(fig)

	#plt.show()

