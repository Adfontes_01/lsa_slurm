#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars
import  scipy.optimize as sop

help_message = 'usage example: python make_corpus_chunks.py -r 0 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in '-h':
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')

	HO = StreamingEigenhashes(JV.oDir)
	model = HO.load_decomp_model()

	path2cov = HO.oDir + HO.sdrTMP + 'StatsDec/'

	HO.open_AM_files()		
	CN = np.zeros((HO.nbr_T,2),dtype=np.uint64)

	for i in range(step):
		ind = step*fr + i
		if ind >= HO.nbr_C:
			break
		print fr,ind,HO.nbr_C

		hash_gen = HO.get_gen_hash_chunk(ind)
		all_vectors = HO.transform_vectors(model,hash_gen)

		for vk in all_vectors:
			fl0=(vk!=0)
			fl1=(vk>0)
			CN[:,0]+=fl0
			CN[:,1]+=fl1

	fname = path2cov + str(fr) + '.npy'
	save_data(CN,fname,fl='npy')

	HO.close_AM_files()		
