#!/usr/bin/env python

import sys,getopt,os,math
from scipy.spatial import distance
import numpy as np
#import math,time
#from lsa_math import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

help_message = "usage example: python demo_kde_cluster.py -c 5 -n 100 -d euclidean\n where: \n c - # of clusters\n n - # of samples in cluster\n d - distance name"


if __name__ == "__main__":

	d = 2
	nb = 100
	c = 10
	ds_name = 'euclidean'

	try:
		opts, args = getopt.getopt(sys.argv[1:],'hc:n:d:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-c'):
			c = int(arg)
		elif opt in ('-n'):
			nb = int(arg)
		elif opt in ('-d'):
			ds_name = arg

	an = np.linspace(0,2*np.pi,100)

	A = np.empty((d,0))
	MS = np.ones(0)
	for i in range(c):
		rd = np.random.rand(2*d)
		CV = 0.01*np.array(np.diag(rd[:d]))
		td = np.random.multivariate_normal(2*(rd[d:]-0.5), CV, nb).T
		A = np.concatenate((A,td),axis=1)
		MS = np.concatenate((MS,i*np.ones(nb)))

	fig = plt.figure()
	for i in range(c):
		fl = MS ==i
		plt.plot(A[0,fl], A[1,fl],'o',color=plt.cm.hsv(float(i)/c))
	plt.plot( 1*np.cos(an), 1*np.sin(an))
	plt.grid()
	plt.draw()
	plt.title('initial generated data: ' + str(c) + ' clusters')



	fig = plt.figure()
	plt.plot(A[0,:], A[1,:],'o')
	plt.plot( 1*np.cos(an), 1*np.sin(an))
	plt.grid()
	plt.draw()
	plt.title('raw data to process')

	d = A.shape[0]
	n = A.shape[1]
	m = int(math.sqrt(n))
	print 'd,n,m:',d,n,m

	F = np.zeros(n,dtype=np.float32)
	S = np.zeros(n,dtype=np.float32)

	print 'find local kernels'
	for cl in xrange(n):
		sys.stdout.write('\r'+str(cl)+' - '+str(n))
		sys.stdout.flush()
		Y = distance.cdist([A[:,cl]], [A[:,i] for i in xrange(n)], ds_name)[0]
		sI = np.argsort(Y)
		sg = float(Y[sI[m]])
		S[cl]=Y[sI[m]]
		F += (np.exp(-(0.5*(Y/sg)**2))/(sg))
	print
	F /= np.max(F)

	'''
	fig = plt.figure()
	plt.plot(np.sort(S))
	plt.draw()
	'''

	fig = plt.figure()
	ax = Axes3D(fig)
	ax.scatter(A[0,:],A[1,:],F)
	plt.plot( 1*np.cos(an), 1*np.sin(an),np.zeros(an.size))
	plt.title('pdf via kde')

	plt.draw()

	B = []

	print 'find local pdf max'
	for cl in xrange(n):
		sys.stdout.write('\r'+str(cl)+' - '+str(n))
		sys.stdout.flush()
		Y = distance.cdist([A[:,cl]], [A[:,i] for i in xrange(n)], ds_name)[0]
		sI = np.argsort(Y)
		sI = sI[:(m+1)]
		tF = [F[i] for i in sI]
		mni = tF.index(max(tF))
		B += [sI[mni]]
	print

	print 'merge data points to locals'
	for cl in xrange(n):
		sys.stdout.write('\r'+str(cl)+' - '+str(n))
		sys.stdout.flush()

		ind = cl
		T = [ind]
		val = F[ind]
		while val < F[B[ind]]:
			T += [B[ind]]
			ind = B[ind]
			val = F[ind]
		for v in T:
			B[v] = T[-1]

	print

	CLS = list(set(B))


	mxCLS = max(CLS)
	fig = plt.figure()
	sz = []
	for cls in CLS:
		fl = []
		for j in xrange(len(B)):
			if B[j] == cls:
				fl += [j]
		sz += [len(fl)]
		plt.plot(A[0,fl], A[1,fl],'o',color=plt.cm.hsv(float(cls)/mxCLS))

	plt.plot( 1*np.cos(an), 1*np.sin(an))
	plt.title('clustered data: ' + str(len(CLS)) + ' clusters')
	plt.grid()
	plt.draw()

	print 'clusters sizes:\n',sz
	plt.show()
