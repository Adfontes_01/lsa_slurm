#!/bin/bash
#MSUB -r ilmap_exemple
#MSUB -q normal
#MSUB -n 1
#MSUB -c 1
#MSUB -o ilmap_exemple_%j.out
#MSUB -e ilmap_exemple_%j.err

module load extenv/rdbioseq
module load mapping

ilmap \
-genome reference/reference.fasta \
-lib A@pairedType \
-f A@AWK_AOSC_4_1_C0BV5ACXX.IND3_10000.fastq.gz@AWK_AOSC_4_2_C0BV5ACXX.IND3_10000.fastq.gz \
-mappingType mem \
-tools samtools \
-index "is" \
-rmUnmapped \
-rmDup 1 \
-bamFilter "-i 95 -a 80"  \
-uniqFilter \
-rpkm \
-mpileup \
-nbrTasks 1 \
-output out



#!/bin/bash
#MSUB -r ilmap_exemple
#MSUB -q normal
#MSUB -n 1
#MSUB -c 1
#MSUB -o ilmap_exemple_%j.out
#MSUB -e ilmap_exemple_%j.err

module load extenv/rdbioseq
module load mapping

ilmap \
-genome /env/cns/bigtmp1/ILMAP/genomes/A/scaffolds_540.0.fasta \
-lib EGAR00001420536@singleType \
-f EGAR00001420536@/env/cns/bigtmp1/ILMAP/samples/1/EGAR00001420536.CH0.fastq.gz \
-f EGAR00001420536@/env/cns/bigtmp1/ILMAP/samples/1/EGAR00001420536.CH1.fastq.gz \
-f EGAR00001420536@/env/cns/bigtmp1/ILMAP/samples/1/EGAR00001420536.CH2.fastq.gz \
-lib EGAR00001420537@singleType \
-f EGAR00001420537@/env/cns/bigtmp1/ILMAP/samples/2/EGAR00001420537.CH67.fastq.gz \
-f EGAR00001420537@/env/cns/bigtmp1/ILMAP/samples/2/EGAR00001420537.CH68.fastq.gz \
-f EGAR00001420537@/env/cns/bigtmp1/ILMAP/samples/2/EGAR00001420537.CH69.fastq.gz \
-mappingType mem \
-tools samtools \
-index "is" \
-rmUnmapped \
-rmDup 1 \
-bamFilter "-i 97 -a 80"  \
-uniqFilter \
-nbrTasks 1 \
-output out
#-rpkm \
#-mpileup \
