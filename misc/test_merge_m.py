import sys,random
import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg as la

sys.path.insert(0, '/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/LSA')
from space_m import *
import mpl_toolkits.mplot3d.art3d as art3d
from lsa_job import *


d=2

n=150
X=np.zeros((d,0),dtype=np.float32)
print X.dtype
for i in range(10):
    s = np.random.multivariate_normal(30*(np.random.rand(d)-0.5),np.diag(0.01+3.0*np.random.rand(d)),n+random.randint(0,n)).T
    print i,'size',s.shape,s.dtype
    X=np.hstack((X,s.astype(np.float32)))

sz = X.shape
print sz

#X=X[:,:4096]
#print X.dtype


#plot2color(X,np.ones(X.shape[1]),tlt='init data')

#sz = X.shape
#sz=[3,4096]
print sz

for i in range(sz[0]):
    save_data(X[i,:],'tmp/X_'+str(i),fl='mm')
    
    xv = load_data('tmp/X_'+str(i),fl='mm',dump=np.float32)
    si = np.argsort(xv)
    save_data(si,'tmp/SI_'+str(i),fl='mm')
    ssi = np.argsort(si)
    save_data(ssi,'tmp/SSI_'+str(i),fl='mm')

del X

X = [load_data('tmp/X_'+str(i),fl='mm',dump=np.float32) for i in range(sz[0])]
SI = [load_data('tmp/SI_'+str(i),fl='mm',dump=np.uint64) for i in range(sz[0])]
SSI = [load_data('tmp/SSI_'+str(i),fl='mm',dump=np.uint64) for i in range(sz[0])]

print len(X),len(X[0]),X[0].shape
m= 1+int(1*np.sqrt(sz[1]))
#plot2color(X,np.ones(sz[1]),tlt='init data')
#plt.show()
#sys.exit()

print sz,m


'''
tI,tV = circle_indx(X,SI,2*sz[0],0)
print len(tI),tI
bI=np.zeros(sz[1],dtype=np.uint16)
bI[tI]=1
bI[0]=2
plot2color(X,bI)

tI,tV = circle_indx_fast(X,SI,2*sz[0],0)
print len(tI),tI
bI=np.zeros(sz[1],dtype=np.uint16)
bI[tI]=1
bI[0]=2
ccccplot2color(X,bI)

tI = circle_indx_direct(X,SI,2*sz[0],0)
print len(tI),tI
bI=np.zeros(sz[1],dtype=np.uint16)
bI[tI]=1
bI[0]=2
plot2color(X,bI)

tI = circle_indx_sort(X,SI,2*sz[0],0)
print len(tI),tI
bI=np.zeros(sz[1],dtype=np.uint16)
bI[tI]=1
bI[0]=2
plot2color(X,bI)

tI = circle_indx_intersect(X,SI,2*sz[0],0)
print len(tI),tI
bI=np.zeros(sz[1],dtype=np.uint16)
bI[tI]=1
bI[0]=2
plot2color(X,bI)

tI = circle_indx_inc(X,SI,SSI,2*sz[0],0)
print len(tI),tI
bI=np.zeros(sz[1],dtype=np.uint16)
bI[tI]=1
bI[0]=2
plot2color(X,bI)

plt.show()
sys.exit()
'''

'''
bI=np.zeros(sz[1],dtype=np.uint16)

plot2color(X,bI)
for j in range(sz[1]):
    tI,tV = circle_indx(X,SI,2*sz[0],j)
    v=np.zeros(2*tI.size+1,dtype=np.uint64)
    v[1::2]=tI
    v[::2] = j
    plt.plot(X[0][v],X[1][v],'k')


plot2color(X,bI)
for j in range(sz[1]):
    tI,tV = circle_indx_fast(X,SI,2*sz[0],j)
    v=np.zeros(2*tI.size+1,dtype=np.uint64)
    v[1::2]=tI
    v[::2] = j
    plt.plot(X[0][v],X[1][v],'k')

plot2color(X,bI)
for j in range(sz[1]):
    tI = circle_indx_direct(X,SI,2*sz[0],j)
    v=np.zeros(2*tI.size+1,dtype=np.uint64)
    v[1::2]=tI
    v[::2] = j
    plt.plot(X[0][v],X[1][v],'k')

plot2color(X,bI)
for j in range(sz[1]):
    tI = circle_indx_sort(X,SI,2*sz[0],j)
    v=np.zeros(2*tI.size+1,dtype=np.uint64)
    v[1::2]=tI
    v[::2] = j
    plt.plot(X[0][v],X[1][v],'k')

plot2color(X,bI)
for j in range(sz[1]):
    tI = circle_indx_intersect(X,SI,2*sz[0],j)
    v=np.zeros(2*tI.size+1,dtype=np.uint64)
    v[1::2]=tI
    v[::2] = j
    plt.plot(X[0][v],X[1][v],'k')

plot2color(X,bI)
for j in range(sz[1]):
    tI = circle_indx_inc(X,SI,SSI,2*sz[0],j)
    v=np.zeros(2*tI.size+1,dtype=np.uint64)
    v[1::2]=tI
    v[::2] = j
    plt.plot(X[0][v],X[1][v],'k')

plt.show()
sys.exit()
'''

nbr = 2*sz[0]
#nbr = m
#IND = ind_mat(X,SI,SSI,nbr)

ln = len(X[0])

pt = np.zeros((sz[0],ln))
si = np.zeros((sz[0],ln),dtype=np.uint64)
ssi = np.zeros((sz[0],ln),dtype=np.uint64)
print pt.dtype,sz,len(X)
for i in range(sz[0]):
    pt[i,:]=X[i][:]
    si[i,:]=SI[i][:]
    ssi[i,:]=SSI[i][:]

#DC = dc_from_2d(pt,nbr)
#IND = ind_mat_connects(DC,pt,nbr)
t = time.time()
IND = ind_mat_2div(pt,nbr)
print time.time() - t
#sys.exit()
t = time.time()
IND = ind_mat_2div_arr(pt,nbr)
print time.time() - t

#plt.show()
#sys.exit()

#'''
plot2color(X,np.zeros(sz[1]))
for j in range(sz[1]):
    #tI = circle_indx_inc_2s(X,SI,SSI,nbr,j)
    tI=IND[:,j]
    #tI = circle_indx_inc(X,SI,SSI,nbr,j)
    #tI = circle_indx_sort(X,SI,nbr,j)
    #tI = circle_indx_intersect(X,SI,nbr,j)
    v=np.zeros(2*tI.size+1,dtype=np.uint64)
    v[1::2]=tI
    v[::2] = j
    if sz[0]==1:
        plt.plot(X[0][v],np.ones(v.size),'k')
    elif sz[0]==2:
        plt.plot(X[0][v],X[1][v],'k')
    else:
        plt.plot(X[0][v],X[1][v],X[2][v],'k')

#'''
#plt.show()
#sys.exit()

'''
bI=np.zeros(sz[1],dtype=np.uint16)
bI[IND[:,0]]=1
bI[0]=2
plot2color(X,bI)

print 'nbr,m',nbr,m
tI,tV =  iter_indx_direct(X,IND,4*m,0)
tI=tI[:m]
print tI.size, tI
bI=np.zeros(sz[1],dtype=np.uint16)
bI[tI]=1
bI[0]=2
plot2color(X,bI)

plt.show()
sys.exit()
'''

print 'search over m',m
SD = np.zeros(sz[1])
for j in range(sz[1]):
    tI,tV =  iter_indx_direct(X,IND,m,j)
    SD[j] = np.sqrt(tV[-1])

#sys.exit()

VI = np.zeros(sz[1],dtype=np.uint64)
#mnb= min(m,3**sz[0])
#mnb= min(m,4*sz[0])
#mnb= max(4*sz[0],int(sz[1]**0.25))
mnb=m

print 'search over mnb',mnb,4*sz[0],int(sz[1]**0.25)
#sys.exit()

for j in range(sz[1]):
    tI,tV =  iter_indx_direct(X,IND,m,j)
    im = np.argmin(SD[tI])
    VI[j] = tI[im]


print 'merge to locals'
for j in range(sz[1]):
    tI = np.array([j],dtype=np.uint64)
    while not np.any(tI == VI[tI[-1]]):
        tI = np.append(tI, VI[tI[-1]])
    VI[tI] = tI[-1]
    #VI[j] = tI[-1]

plot2color(X,VI,tlt='merge to locals')

print 'filter connections'
LS = xrange(VI.size)
uv,un,D = uv_un_D(VI)
print np.sort(un)
pnc = -1
cnc = 0
while pnc != cnc:
        pnc = cnc
        print 'next LS',len(LS)

        VI = mat_filter_connects(X,IND,VI,LS,mnb)
        plot2color(X,VI,tlt='filter_connects')

        uv,un,D = uv_un_D(VI)
        cnc = uv.size
        if np.sum(un<=m):

            print 'count connections',mnb
            CN = mat_count_connects(X,IND,VI,LS,mnb)
            
            print 'mat_merge_small',m
            VI = mat_merge_small(VI,CN,m)
            plot2color(X,VI,tlt='merge_small')

            uv,un,D = uv_un_D(VI)
            print np.sort(un)
            cnc = uv.size
        break
print 'count connections',mnb
CN = mat_count_connects(X,IND,VI,LS,mnb)
CN = threshold_connects(CN,len(X))

print 'gauss_mask_dists'
CN = gauss_mask_dists(X,VI,CN)

print 'gauss merge clusters'
VI= gauss_merge_connected(X,VI,CN)
uv,un,D = uv_un_D(VI)
print uv.size, un
plot2color(X,VI,tlt='merge_connected')


plt.show()

